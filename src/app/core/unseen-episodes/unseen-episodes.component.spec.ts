import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnseenEpisodesComponent } from './unseen-episodes.component';

describe('UnseenEpisodesComponent', () => {
  let component: UnseenEpisodesComponent;
  let fixture: ComponentFixture<UnseenEpisodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnseenEpisodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnseenEpisodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
