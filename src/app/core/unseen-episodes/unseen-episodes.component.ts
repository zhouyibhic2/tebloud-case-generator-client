import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-unseen-episodes',
  templateUrl: './unseen-episodes.component.html',
  styleUrls: ['./unseen-episodes.component.scss'],
})
export class UnseenEpisodesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.tvShows = this.tvShowsTemp;
      setTimeout(() => this.tvShows[0].episodes = this.episodesTemp, 1000);
    }, 500);
  }

  episodesTemp = [
    {
      title: 'Vacation Itinerary',
      season: 1,
      number: 15,
      date: new Date('1/1/9'),
      state: 'active'
    },
    {
      title: 'Kitchen Remodel',
      season: 1,
      number: 16,
      date: new Date('1/1/16'),
      state: 'active'
    },
    {
      title: 'Vacation Itinerary',
      season: 1,
      number: 17,
      date: new Date('1/1/9'),
      state: 'active'
    },
    {
      title: 'Kitchen Remodel',
      season: 1,
      number: 18,
      date: new Date('1/1/16'),
      state: 'active'
    },
    {
      title: 'Vacation Itinerary',
      season: 1,
      number: 19,
      date: new Date('1/1/9'),
      state: 'active'
    },
    {
      title: 'Kitchen Remodel',
      season: 1,
      number: 20,
      date: new Date('1/1/16'),
      state: 'active'
    },
    {
      title: 'Vacation Itinerary',
      season: 2,
      number: 1,
      date: new Date('1/1/9'),
      state: 'active'
    },
    {
      title: 'Kitchen Remodel',
      season: 2,
      number: 2,
      date: new Date('1/1/16'),
      state: 'active'
    },
    {
      title: 'Vacation Itinerary',
      season: 2,
      number: 3,
      date: new Date('1/1/9'),
      state: 'active'
    },
    {
      title: 'Kitchen Remodel',
      season: 2,
      number: 4,
      date: new Date('1/1/16'),
      state: 'active'
    }
  ];
  tvShows = [];
  tvShowsTemp = [
    {
      name: 'Stargate',
      episodes: [],
      state: 'active'
    },
    {
      name: 'Attack on Titan',
      episodes: [

      ],
      state: 'active'
    },
    {
      name: 'The Magicians',
      episodes: [

      ],
      state: 'active'
    },
    {
      name: 'Healer',
      episodes: [

      ],
      state: 'active'
    }
  ];

  addTvShow() {
    this.tvShows.splice(1, 0, {
      name: 'Test1212',
      episodes: [
      ],
      state: 'active'
    });
  }

}
