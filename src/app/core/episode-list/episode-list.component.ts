import {Component, Input, OnInit} from '@angular/core';
import {listStaggeredInOut} from "../../../animations/list-staggered-in-out.animation";

@Component({
  selector: 'app-episode-list',
  templateUrl: './episode-list.component.html',
  styleUrls: ['./episode-list.component.scss'],
  animations: [listStaggeredInOut]
})
export class EpisodeListComponent implements OnInit {

  @Input() episodes: any;

  constructor() { }

  ngOnInit() {
  }

  onEpisodeChecked(episode) {
    let index = this.episodes.indexOf(episode);
    this.episodes.splice(index, 1);
  }

}
