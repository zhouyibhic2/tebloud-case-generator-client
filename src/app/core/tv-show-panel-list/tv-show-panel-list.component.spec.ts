import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TvShowPanelListComponent } from './tv-show-panel-list.component';

describe('TvShowPanelListComponent', () => {
  let component: TvShowPanelListComponent;
  let fixture: ComponentFixture<TvShowPanelListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TvShowPanelListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TvShowPanelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
