import {Component, Input, OnInit} from '@angular/core';
import {listStaggeredInOut} from "../../../animations/list-staggered-in-out.animation";

@Component({
  selector: 'app-tv-show-panel-list',
  templateUrl: './tv-show-panel-list.component.html',
  styleUrls: ['./tv-show-panel-list.component.scss'],
  animations: [listStaggeredInOut],
})
export class TvShowPanelListComponent implements OnInit {

  @Input() tvShows: any;

  constructor() { }

  ngOnInit() {
  }

}
