import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: '[app-episode]',
  templateUrl: './episode.component.html',
  styleUrls: ['./episode.component.scss']
})
export class EpisodeComponent implements OnInit {

  @Input() episode: any;
  @Output() episodeChecked = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  checkEpisode() {
    this.episodeChecked.emit(this.episode);
  }
}
