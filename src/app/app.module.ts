import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule, MatSidenavModule, MatListModule, MatExpansionModule, MatInputModule, MatButtonModule, MatCheckboxModule } from '@angular/material';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { UnseenEpisodesComponent } from './core/unseen-episodes/unseen-episodes.component';
import { TvShowPanelListComponent } from './core/tv-show-panel-list/tv-show-panel-list.component';
import { EpisodeListComponent } from './core/episode-list/episode-list.component';
import { EpisodeComponent } from './core/episode/episode.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    AppComponent,
    UnseenEpisodesComponent,
    EpisodeListComponent,
    EpisodeComponent,
    TvShowPanelListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
    	loader: {
    		provide: TranslateLoader,
	      useFactory: HttpLoaderFactory,
	      deps: [HttpClient]
    	}
    }),

    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatExpansionModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
