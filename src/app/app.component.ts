import {Component, OnInit} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

	constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');
  }

  ngOnInit() {
  }

  isDarkTheme: boolean = false;

  switchLanguage(language: string) {
    this.translate.use(language);
  }

  changeTheme(): void {
    this.isDarkTheme = !this.isDarkTheme;
  }
}
