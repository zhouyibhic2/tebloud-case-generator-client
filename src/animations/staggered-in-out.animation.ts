import {trigger, animate, transition, style, query, stagger, keyframes} from '@angular/animations';

export const staggeredInOut =
  trigger('staggeredInOut', [
    transition('* => *', [

      query('*', style({ opacity: 0 })),
// grab a bunch of things and then animate
      query('div, .inner, #id', [
        animate(1000, style({ opacity: 1 }))
      ]),
// then reset the state for each element
      query('*', [
        animate(1000, style('*'))
      ]),

    ])
  ]);