import {trigger, animate, transition, style, query, stagger, keyframes, group} from '@angular/animations';

export const listStaggeredInOut =
  trigger('listStaggeredInOut', [
    transition('* => *', [

      query(':enter', style({ height: 0 }), { optional: true }),

      group([
        query(':leave', stagger('100ms', [
          animate('200ms ease', keyframes([
            style({ height: '*', opacity: 1, transform: 'translateX(0)', offset: 0 }),
            style({ height: 0, opacity: 0, transform: 'translateX(20%)', offset: 1.0 }),
          ]))
        ]), { optional: true }),
        query(':enter', stagger('100ms', [
          animate('200ms ease', keyframes([
            style({ height: 0, opacity: 0, transform: 'translateX(-20%)', offset: 0 }),
            style({ height: '*', opacity: 1, transform: 'translateX(0)', offset: 1.0 }),
          ]))
        ]), { optional: true }),
      ])
    ])
  ]);