import {trigger, animate, transition, style, sequence} from '@angular/animations';

export const listElementInOut =
  trigger('listElementInOut', [
    transition('* => void', [
      style({ height: '*', opacity: '1', transform: 'translateX(0)'}),
      sequence([
        animate(".25s ease", style({ height: '*', opacity: '.2', transform: 'translateX(20px)' })),
        animate(".1s ease", style({ height: '0', opacity: 0, transform: 'translateX(20px)' }))
      ])
    ]),
    transition('void => *', [
      style({ height: '0', opacity: '0', transform: 'translateX(20px)' }),
      sequence([
        animate(".1s ease", style({ height: '*', opacity: '.2', transform: 'translateX(20px)' })),
        animate(".35s ease", style({ height: '*', opacity: 1, transform: 'translateX(0)' }))
      ])
    ])
  ]);