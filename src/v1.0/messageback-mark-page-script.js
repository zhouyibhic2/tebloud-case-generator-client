// document.querySelector('button').addEventListener('click', function() {
//  chrome.extension.sendMessage({action: 'message', content:"Changed by page"}, function(message){});
// });
/*document.querySelector('button').addEventListener('click', function() {
 sendObjectToDevTools({content: "Changed by page"});
 });*/
//alert(ate_jQuery_3_0_0.fn.jquery);

var ate_marker_script_execution_ended = false;
function sendObjectToDevTools(message) {
    // The callback here can be used to execute something on receipt
    chrome.extension.sendMessage(message, function (message) {
    });
}
function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};
function appliedProperty(element, propString) {
    var notAppliedMap = {"form": ["height", "width"]};
    var retVal = true;
    Object.keys(notAppliedMap).forEach(function(elementTag){
        if (element.tagName.toLowerCase() == elementTag) {
            if (ate_jQuery_3_0_0.inArray(element.tagName.toLowerCase(), notAppliedMap[elementTag])) {
                retVal = false;
            }
        }
    })
    return retVal;

}

//modalGuid == "", means modal not displayed. all invisibles will be marked normally
function ate_add_markers_with_modal_on_page(domdoc, frameTop, frameLeft, visibleModalGuid, isDiaplyedModalChildFrame, parentFrameInvisible) {
    var visibleModalFound = false;
    if (visibleModalGuid=="" || isDiaplyedModalChildFrame || parentFrameInvisible) {
        ate_add_all_markers(domdoc, frameTop,frameLeft, parentFrameInvisible);
    } else {
        ate_add_guid_size_xpath_attrs(domdoc, frameTop, frameLeft);
        var modalElems = [];
        modalElems = ate_jQuery_3_0_0(ate_get_body_equavelent_element(domdoc)).find("[ate-guid='"+visibleModalGuid+"']");
        var visibleModalElem = null;
        ate_jQuery_3_0_0.each(modalElems, function(mdlIndex, mdl){
            if (!ate_this_element_is_invisible(mdl)) {
                visibleModalFound= true;
                visibleModalElem = mdl;
            }
        });
        if (visibleModalElem != null) {
            ate_jQuery_3_0_0.each(ate_jQuery_3_0_0(ate_get_body_equavelent_element(domdoc)).find("*"), function (ellmIndex, ellm) {
                if (!(ate_jQuery_3_0_0(visibleModalElem).find(ellm).length>0 || ellm === visibleModalElem || ate_jQuery_3_0_0.contains(ellm, visibleModalElem))) {
                    ellm.setAttribute("ate-invisible", "yes");
                } else {
                    if (ate_this_element_is_invisible(ellm)) {
                        ellm.setAttribute("ate-invisible", "yes");
                    }
                }
            });

        } else {
            ate_jQuery_3_0_0.each(ate_jQuery_3_0_0(ate_get_body_equavelent_element(domdoc)).find("*"), function (ellmIndex, ellm) {
                ellm.setAttribute("ate-invisible", "yes");
            })
        }
    }
    return visibleModalFound;
}
function ate_this_element_is_invisible(elem) {
    var width=Math.round(ate_jQuery_3_0_0(elem).width());
    var height = Math.round(ate_jQuery_3_0_0(elem).height());
    var leftTopCoord = ate_jQuery_3_0_0(elem).offset();

    if (leftTopCoord.left < 0 && leftTopCoord.left + width <0) return true;
    if (leftTopCoord.left > window.innerWidth ) return true;


    if (ate_jQuery_3_0_0(elem).is(":hidden") || ate_jQuery_3_0_0(elem).css("visibility")=="hidden"
        || (ate_jQuery_3_0_0(elem).attr("aria-hidden")== "true" && ate_jQuery_3_0_0(elem).css("display") !== "block")
        || (ate_jQuery_3_0_0(elem).css("opacity")== 0 && ate_jQuery_3_0_0(elem).css("-webkit-appearance") === "none") || (leftTopCoord.left < 0 && leftTopCoord.left + width <0) || (leftTopCoord.left > window.innerWidth ) ||
    (leftTopCoord.top<0 && leftTopCoord.top + height<0)) {

        if (ate_jQuery_3_0_0(elem).css("-webkit-appearance") === "none")
            return true;
        else
            return false;
    }
    else
        return false;
}

function ate_this_document_has_modal_displayed(domdoc) {

    var ate_modalElements = [];
    ate_modalElements = ate_jQuery_3_0_0(ate_get_body_equavelent_element(domdoc)).find("div[role='dialog'], div[class*='modal-box']");
    //TODO z-index case, usually this type of dialog has z-index>1 and width is whole screen width, cornered at 0,0 and height is greater than 500
    //this is an example,
    /*
     <div class="WJLL WGLL" style="z-index: 50;" ate-guid="2167843a-65d9-49ad-8647-60816d0d4a2a" ate-computed-width="1280"
     ate-computed-height="934" ate-computed-lefttopcornerx="0" ate-computed-lefttopcornery="0"><div class="WHLL" ate-guid="a26b5882-aef5-42e6-8e9b-f8de05887c97" ate-computed-width="1280" ate-computed-height="934" ate-computed-lefttopcornerx="0" ate-computed-lefttopcornery="0"><div class="WFLL" ate-guid="e7f4d51f-c2db-4675-b2a8-d771dd1339ca" ate-computed-width="1280" ate-computed-height="934" ate-computed-lefttopcornerx="0" ate-computed-lefttopcornery="0">
     </div>
     */
    var ate_modalByZIndexElement;
    ate_jQuery_3_0_0(ate_get_body_equavelent_element(domdoc)).find("*").filter(function() {
        return parseInt(ate_jQuery_3_0_0(this).css('z-index'))>=1
            && parseInt(ate_jQuery_3_0_0(this).attr("ate-computed-width"))>=ate_jQuery_3_0_0(document).width()
            && parseInt(ate_jQuery_3_0_0(this).attr("ate-computed-lefttopcornerx"))==0
            && parseInt(ate_jQuery_3_0_0(this).attr("ate-computed-lefttopcornery"))==0
            && parseInt(ate_jQuery_3_0_0(this).attr("ate-computed-height"))>=500;
    }).each(function() {
        if (ate_modalByZIndexElement) {
            if (parseInt(ate_jQuery_3_0_0(this).css('z-index'))>parseInt(ate_jQuery_3_0_0(ate_modalByZIndexElement).css('z-index')))
                ate_modalByZIndexElement = this;
        } else {
            ate_modalByZIndexElement = this;
        }
    });
    if (ate_modalByZIndexElement)
        ate_modalElements.push(ate_modalByZIndexElement);
    ate_modalElements = ate_modalElements.filter(function(elemKey, elem){
        return !ate_this_element_is_invisible(elem);
    });
    if (ate_modalElements.length>0) {
        return ate_modalElements[0].getAttribute("ate-guid");
    } else {

        return "";
    }
}
function ate_this_page_has_modal_displayed(topDomdoc) {
    var modalGuid = "";
    if ((modalGuid = ate_this_document_has_modal_displayed(topDomdoc))!="") {
        return modalGuid;
    } else {
        var allFrameNodes = ate_parseAllFrames(topDomdoc);
        for (var index=0; index<allFrameNodes.length; index++) {
            modalGuid = ate_this_page_has_modal_displayed(allFrameNodes[index].contentWindow.document);
            if (modalGuid!="") {
                return modalGuid;
            }
        }
        return "";
    }
}


function ate_add_all_markers(document, frameTop, frameLeft, parentFrameInvisible) {
    ate_add_guid_size_xpath_attrs(document, frameTop, frameLeft);

    if (parentFrameInvisible) {
        var ate_allElements = ate_jQuery_3_0_0(ate_get_body_equavelent_element(document)).find("*");
        ate_jQuery_3_0_0.each(ate_allElements, function(elemIndex, elem) {
            elem.setAttribute("ate-invisible", "yes");
        })
    } else {

        var ate_hiddenElements = [];
        ate_hiddenElements = []; //ate_jQuery_3_0_0(ate_get_body_equavelent_element(document)).find(":hidden,[aria-hidden='true'][display!='block']");
        ate_CssInvisibleElements = ate_jQuery_3_0_0(ate_get_body_equavelent_element(document)).find("*").filter(function(elemIndex, elem) {

                return ate_this_element_is_invisible(elem);
            }
        );
        var ate_CssOpacity0Elements =[];

        var ate_OutOfViewElements =[];

        var ate_CssHeightWidth0Elements = [];

        ate_hiddenElements = ate_jQuery_3_0_0.merge(ate_jQuery_3_0_0.merge(ate_jQuery_3_0_0.merge(ate_jQuery_3_0_0.merge(ate_hiddenElements, ate_CssInvisibleElements), ate_CssOpacity0Elements), ate_CssHeightWidth0Elements), ate_OutOfViewElements);
        for (var ate_hiddenIndex = 0; ate_hiddenIndex < ate_hiddenElements.length; ate_hiddenIndex++) {
            ate_hiddenElements[ate_hiddenIndex].setAttribute("ate-invisible", "yes");
            ate_jQuery_3_0_0(ate_hiddenElements[ate_hiddenIndex]).find("*").each(function(){
                this.setAttribute("ate-invisible", "yes");
            })
        }

    }
}

function ate_removeJSStypleAndCloneDoc(doc) {

    var cloned = ate_jQuery_3_0_0(doc).find("html").clone()[0];
    var arr_scripts = ate_jQuery_3_0_0(cloned).find("script");
    var arr_styles = ate_jQuery_3_0_0(cloned).find("style");
    var arr_links = ate_jQuery_3_0_0(cloned).find("link");
    ate_jQuery_3_0_0.each(arr_scripts,function(eachIndex, eachValue) {
        eachValue.remove();
    })
    ate_jQuery_3_0_0.each(arr_styles,function(eachIndex, eachValue) {
        eachValue.remove();
    })
    ate_jQuery_3_0_0.each(arr_links,function(eachIndex, eachValue) {
        eachValue.remove();
    })
    return cloned;
}

function ate_set_guid_size_loc_attributes(element, frameTop, frameLeft) {
    if (!element.hasAttribute("ate-guid"))
        element.setAttribute("ate-guid", generateUUID());
    element.setAttribute("ate-computed-width", Math.round(ate_jQuery_3_0_0(element).width()));
    element.setAttribute("ate-computed-height", Math.round(ate_jQuery_3_0_0(element).height()));
    var leftTopCoord = ate_jQuery_3_0_0(element).offset();
    if (leftTopCoord.left !== "undefined") {
        element.setAttribute("ate-computed-leftTopCornerX", Math.round(leftTopCoord.left) + Math.round(frameLeft));
        element.setAttribute("ate-computed-leftTopCornerY", Math.round(leftTopCoord.top) + Math.round(frameTop));
    } else {
        element.setAttribute("ate-computed-leftTopCornerX", Math.round(frameLeft));
        element.setAttribute("ate-computed-leftTopCornerY", Math.round(frameTop));
    }
}
function ate_set_guid_size_loc_xpath_attributes_recursively(element, frameTop, frameLeft) {
    if (element.nodeType == 1) {
        ate_set_guid_size_loc_attributes(element, frameTop, frameLeft);
        ate_set_xpath_attributes(element, frameTop, frameLeft)
    }
    ate_jQuery_3_0_0.each(ate_jQuery_3_0_0(element).find("*"), function(index, node) {
        if (node.nodeType == 1) {

            ate_set_guid_size_loc_attributes(node, frameTop, frameLeft);
            ate_set_xpath_attributes(node, frameTop, frameLeft)
        }
    });
}
function ate_set_xpath_attributes(element, frameTop, frameLeft) {
    if (!element.hasAttribute("ate-guid"))
        element.setAttribute("ate-guid", generateUUID());
    element.setAttribute("ate-computed-xpath", getElementXPath(element));

}
function ate_get_body_equavelent_element(doc) {
    if (typeof doc.body != "undefined") {
        //document.body either return frameset or body element in chrome
        return doc.body;
    }
    else if (typeof doc.getElementsByTagName("frameset") != "undefined" && doc.getElementsByTagName("frameset").length==1) {
        return doc.getElementsByTagName("frameset")[0];
    } else {
        return doc.getElementsByTagName("body")[0];
    }
}
function ate_add_guid_size_xpath_attrs(document, frameTop, frameLeft) {
    if (!ate_get_body_equavelent_element(document).hasAttribute("ate-guid"))
        ate_set_guid_size_loc_attributes(ate_get_body_equavelent_element(document), frameTop, frameLeft);

    var elems = ate_get_body_equavelent_element(document).getElementsByTagName("*");
    for (var ate_guidIndex=0; ate_guidIndex < elems.length; ate_guidIndex++) {
        ate_set_guid_size_loc_attributes(elems[ate_guidIndex], frameTop, frameLeft);
        ate_set_xpath_attributes(elems[ate_guidIndex], frameTop, frameLeft);
    }
}

function ate_add_mutation_observer_to_document(doc) {
    var MutationObserver    = window.MutationObserver || window.WebKitMutationObserver;
    var myObserver          = new MutationObserver (ate_mutationHandler_add_guid_size_xpath_attrs);
    var obsConfig           = { childList: true, characterData: false, attributes: false, subtree: true };

//--- Add a target node to the observer. Can only add one node at a time.
    myObserver.observe (ate_get_body_equavelent_element(doc), obsConfig);

}

function ate_mutationHandler_add_guid_size_xpath_attrs (mutationRecords) {
    mutationRecords.forEach ( function (mutation) {
        if (typeof mutation.addedNodes == "object") {
            //var nodes = ate_jQuery_3_0_0(mutation.addedNodes);
            var bodyGuid = ate_jQuery_3_0_0(document.body).attr("ate-guid");
            var frameEntities = ate_jQuery_3_0_0.grep(ate_ml_allDocs_in_page, function(frame, ind) {
                return frame.docGuid === bodyGuid;
            });
            if (frameEntities.length == 1) {

                    ate_jQuery_3_0_0.each(mutation.addedNodes, function (index, node) {

                        ate_set_guid_size_loc_xpath_attributes_recursively(node, frameEntities[0].locationTop, frameEntities[0].locationLeft);

                    });
            }

        }
    });
}

/*function ate_add_clickable_attribute() {
 $( "body" ).each(function( index ) {
 var events = ($._data || $.data)(this, 'events');
 for (i=0; i<events.length; i++) {
 if (events[i].type === 'click') {
 this.setAttribute('ate-clickable', 'yes');
 }
 }
 //console.log( index + ": " + $( this ).text() );
 });
 }*/
function ate_remove_ate_invisible_marker(document) {
    var ate_invisibleElements = ate_jQuery_3_0_0(ate_get_body_equavelent_element(document)).find("[ate-invisible='yes']");
    for (var ate_invisibleIndex = 0; ate_invisibleIndex < ate_invisibleElements.length; ate_invisibleIndex++) {
        ate_invisibleElements[ate_invisibleIndex].setAttribute("ate-invisible", "no");
    }
}
function ate_mark_all_elem_invisible(document) {
    var ate_invisibleElements = ate_jQuery_3_0_0(ate_get_body_equavelent_element(document)).find("*");
    for (var ate_invisibleIndex = 0; ate_invisibleIndex < ate_invisibleElements.length; ate_invisibleIndex++) {
        ate_invisibleElements[ate_invisibleIndex].setAttribute("ate-invisible", "yes");
    }

}
getAllClickBindElements = function (documentE) {
    //TODO this needs to be working for elements bind with jquery click event
    var allelmts = ate_jQuery_3_0_0(ate_get_body_equavelent_element(documentE)).find("*");
    for (var i = 0; i < allelmts.length; i++) {
        var tempEvents = ate_jQuery_3_0_0._data(allelmts[i], "events");
        if (typeof tempEvents !== "undefined") {
            for (var j = 0; j < tempEvents.length; j++) {
                alert("ab");
                var oneevent = tempEvents[j];
                alert(oneevent.type);
                //for (var k=0; k<
            }
        }
    }
    //return temp;
}
getAllClickablesOnPage = function (docElmt) {
    var allClickables1 = ate_jQuery_3_0_0(docElmt).find("[onclick]");
    var allClickables2 = ate_jQuery_3_0_0(docElmt).find("input[type=button],input[type=submit],input[type=reset], button, a");
    var allClickables = allClickables1.add(allClickables2).add(getAllClickBindElements(docElmt));
    var offset = ate_ml_allClickables_in_page.length;
    for (var i = offset; i < allClickables.length; i++) {
        var invisibleAncestor = ate_jQuery_3_0_0(allClickables[i - offset]).closest("[ate-invisible='yes']");
        if (allClickables[i - offset].getAttribute("ate-invisible") !== "yes" && invisibleAncestor.length===0) {
            var tmp = {clickable: allClickables[i - offset].outerHTML};
            if (ate_jQuery_3_0_0.inArray(tmp, ate_ml_allClickables_in_page) === -1) {
                ate_ml_allClickables_in_page[i] = tmp;
            }
        }
    }
    ;
    //var offset = ate_ml_allClickables_in_page.length;

}

ate_parseAllFrames = function(rootDomDoc) {
    var iframeElements=[];
    var frameElements=[];
    var allFrameNodes=[];
    var frameDoc;

    iframeElements = Array.prototype.slice.call(rootDomDoc.getElementsByTagName("iframe"), 0);
    frameElements = Array.prototype.slice.call(rootDomDoc.getElementsByTagName("frame"), 0);

    allFrameNodes = iframeElements.concat(frameElements);
    return allFrameNodes;
}

//getAllClickablesOnPage(document.documentElement);
ate_markAllDocumentsOnPage = function (thisDocument, parentDocIndex, startingIndex, parentInvisible, thisIsDisplayedModalChildiFrameDoc) {
    ate_add_mutation_observer_to_document(thisDocument);
    var allFrameNodes = ate_parseAllFrames(thisDocument);
    for (var i = 0; i < allFrameNodes.length; i++) {
        var frameInvisible = false;
        if (parentInvisible == true) {
            frameInvisible = true;
        } else {
            frameInvisible = (allFrameNodes[i].getAttribute("ate-invisible") === "yes") ? true : false;
            if (frameInvisible === false) {
                var invisibleAncestor = ate_jQuery_3_0_0(allFrameNodes[i]).closest("[ate-invisible='yes']");
                frameInvisible = (invisibleAncestor.length === 1) ? true : false;
            }
        }
        frameDoc = allFrameNodes[i].contentWindow.document;

        var frameTop = Math.round(ate_jQuery_3_0_0(allFrameNodes[i]).offset().top) + Math.round(ate_ml_allDocs_in_page[parentDocIndex].locationTop);
        var frameLeft = Math.round(ate_jQuery_3_0_0(allFrameNodes[i]).offset().left) + Math.round(ate_ml_allDocs_in_page[parentDocIndex].locationLeft);


        var modalChildiFrame = false;
        if (ate_this_page_displaying_modal_guid != "" && !thisIsDisplayedModalChildiFrameDoc) {
            modalChildiFrame = ate_jQuery_3_0_0(thisDocument).find("[ate-guid='" + ate_this_page_displaying_modal_guid + "']").find(allFrameNodes[i]).length > 0

        } else if (thisIsDisplayedModalChildiFrameDoc) {
            modalChildiFrame = true;
        }
        if (!frameInvisible) {
            ate_remove_ate_invisible_marker(frameDoc);

        }
        ate_add_markers_with_modal_on_page(frameDoc, frameTop, frameLeft, ate_this_page_displaying_modal_guid, modalChildiFrame, frameInvisible);

        ate_ml_allDocs_in_page[i + startingIndex] = {
            index: i + startingIndex,
            parentIndex: parentDocIndex,
            visible: (frameInvisible === true) ? false : true,
            xpathOfFrame: getElementXPath(allFrameNodes[i]),
            domDoc: ate_removeJSStypleAndCloneDoc(frameDoc).outerHTML,//frameDoc.documentElement.outerHTML,
            docText: getText(ate_get_body_equavelent_element(frameDoc)).replace(/\s\s+/g, ' '),
            frameSrc: allFrameNodes[i].getAttribute("src"),
            docGuid: ate_get_body_equavelent_element(frameDoc).getAttribute("ate-guid"),
            locationTop: frameTop,
            locationLeft: frameLeft,
            docWidth:ate_jQuery_3_0_0(frameDoc.documentElement).width(),
            docHeight:ate_jQuery_3_0_0(frameDoc.documentElement).height(),
            screenWidth:window.screen.availWidth
        };
        var tempLength;
        tempLength = ate_ml_allDocs_in_page.length;
        ate_markAllDocumentsOnPage(frameDoc.documentElement, i + startingIndex, i + startingIndex + 1, frameInvisible, modalChildiFrame);
        startingIndex = ate_ml_allDocs_in_page.length - tempLength + startingIndex;

    }
}




function getMarkedPagesInJson() {
    if (ate_global_devTool_back_message !== 'undefined') {
        return JSON.stringify(ate_global_devTool_back_message);
    } else {
        return "";
    }
}


function getText(bodyElement) {
    var elements = bodyElement.getElementsByTagName("*");
    var retVal = " ";
    for (var i = 0; i < elements.length; i++) {
        var current = elements[i];
        if (current.children.length === 0 && current.textContent.replace(/ |\n/g, '') !== '') {
            // Check the element has no children && that it is not empty
            if (current.tagName.toLowerCase() != 'script' && current.tagName.toLowerCase() != 'style')
                retVal = retVal + " " + (current.textContent);
        }
    }
    return retVal;
}

/**
 * Gets an XPath for an element which describes its hierarchical location.
 */
function getElementXPath(element)
{
    if (element && element.id)
        return '//*[@id="' + element.id + '"]';
    else
        return getElementTreeXPath(element);
};

function getElementTreeXPath(element)
{
    var paths = [];

    // Use nodeName (instead of localName) so namespace prefix is included (if any).
    for (; element && element.nodeType == 1; element = element.parentNode)
    {
        var index = 0;
        for (var sibling = element.previousSibling; sibling; sibling = sibling.previousSibling)
        {
            // Ignore document type declaration.
            if (sibling.nodeType == Node.DOCUMENT_TYPE_NODE)
                continue;

            if (sibling.nodeName == element.nodeName)
                ++index;
        }

        var tagName = element.nodeName.toLowerCase();
        var pathIndex = (index ? "[" + (index+1) + "]" : "");
        paths.splice(0, 0, tagName + pathIndex);
    }

    return paths.length ? "/" + paths.join("/") : null;
};

function getAllElementsInBody(bodyElement)
{
    ate_ml_allElements_in_page = bodyElement.getElementsByTagName("*");


};

function getLocationOffsetToWholeDocument(elem) { //or use jQuery offset()
    // (1)
    var box = elem.getBoundingClientRect()

    var body = ate_get_body_equavelent_element(document)
    var docElem = document.documentElement

    // (2)
    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft

    // (3)
    var clientTop = docElem.clientTop || body.clientTop || 0
    var clientLeft = docElem.clientLeft || body.clientLeft || 0

    // (4)
    var top  = box.top +  scrollTop - clientTop
    var left = box.left + scrollLeft - clientLeft

    return { top: Math.round(top), left: Math.round(left), bottom: Math.round(box.bottom + scrollTop - clientTop), right: Math.round(box.right+ scrollLeft - clientLeft) }
};
ate_remove_ate_invisible_marker(document);
ate_add_guid_size_xpath_attrs(document, 0, 0);
ate_add_mutation_observer_to_document(document);

var ate_this_page_displaying_a_modal = false;
var ate_this_page_diaplayed_modal_found = false;
var ate_this_page_displaying_modal_guid = "";
if ((ate_this_page_displaying_modal_guid = ate_this_page_has_modal_displayed(document)) !="") {
    ate_this_page_displaying_a_modal = true;
    if (ate_add_markers_with_modal_on_page(document, 0, 0, ate_this_page_displaying_modal_guid, false, false))
        ate_this_page_diaplayed_modal_found = true;
} else {
    ate_add_all_markers(document, 0, 0, false);
}



//add this meta to avoid the mixed content warning in jquery clone, which slows down the system
//this meta causes wordpress login gives ssl connection error. disable it for now.
//ate_jQuery_3_0_0('meta[content=upgrade-insecure-requests]').remove();
//ate_jQuery_3_0_0('head').append( '<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">' );

ate_ml_allDocs_in_page = [{
    index: 0,
    parentIndex: 0,
    xpathOfFrame: "path0",
    visible: true,
    domDoc: ate_removeJSStypleAndCloneDoc(document).outerHTML,//document.documentElement.outerHTML,
    docText: getText(ate_get_body_equavelent_element(document)).replace(/\s\s+/g, ' '),
    frameSrc: "",
    docGuid: ate_get_body_equavelent_element(document).getAttribute("ate-guid"),
    locationTop: Math.round(ate_jQuery_3_0_0(document.documentElement).offset().top),
    locationLeft: Math.round(ate_jQuery_3_0_0(document.documentElement).offset().left),
    docWidth:ate_jQuery_3_0_0(document).width(),
    docHeight:ate_jQuery_3_0_0(document).height(),
    screenWidth: window.screen.availWidth
}];
ate_ml_allClickables_in_page = [];

ate_ml_allElements_in_page = [];


ate_markAllDocumentsOnPage(document.documentElement, 0, 1, false);
//getAllElementsInBody(document.body);

ate_global_devTool_back_message = {
    content: {
        screenWidth: window.screen.availWidth,
        topDocumentHeight:ate_jQuery_3_0_0(document).height(),
        topDocumentWidth:ate_jQuery_3_0_0(document).width(),
        pages: ate_ml_allDocs_in_page,
        //allClickables: ate_ml_allClickables_in_page,
        screenUrl: window.location.href.replace(/^.*\/\/[^\/]+/, ''),
        domain: window.location.host,
        domainProtocol: window.location.protocol,
        domainPort: window.location.port,
        //allElementsInBody: ate_ml_allElements_in_page
    }
};

//ate_jQuery_3_0_0('meta[http-equiv=Content-Security-Policy]').remove();
//ate_jQuery_3_0_0('head').append( '<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">' );

if (typeof ate_global_webdriver_executed_js === 'undefined')
    sendObjectToDevTools(ate_global_devTool_back_message);

var ate_marker_script_execution_ended = true;
/*else
 //this return can't be accepted by chrome browser in chrome extension injection.
 //we will add this statement in java code.
 return getMarkedPagesInJson();
 */
//http://stackoverflow.com/questions/17727977/how-to-get-all-text-from-all-tags-in-one-array to get the text on the page. need to iterate the frames
