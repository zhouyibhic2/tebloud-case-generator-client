(function () {
    'use strict';

    var app1 = angular.module('app', ['angularSpinner', "ngTouch", "angucomplete-alt", "ngSanitize", "RecursionHelper", "ngStorage", "LZString", "ui.bootstrap", 'ngRoute', 'ngCookies'])
        .service('UserService', function($localStorage) {
            var service = this,
                currentUser = null;
            service.setCurrentUser = function(user) {
                currentUser = user;
                $localStorage.user = user;
                return currentUser;
            };
            service.getCurrentUser = function() {
                if (!currentUser) {
                    currentUser = $localStorage.user;
                }
                return currentUser;
            };
        })
        .factory('AuthInterceptor', function(UserService) {
            return {
                // Send the Authorization header with each request
                'request': function(config) {
                    config.headers = config.headers || {};
                    if (UserService.getCurrentUser())
                        var encodedString = btoa(UserService.getCurrentUser().username + ":" + UserService.getCurrentUser().password);
                    else
                        var encodedString = btoa("");
                    config.headers.Authorization = 'Basic '+encodedString;
                    return config;
                }
            };
        })
        .config(['$httpProvider', function($httpProvider) {
            $httpProvider.interceptors.push('AuthInterceptor');

            //$httpProvider.interceptors.push('responseInterceptor');
        }]);
        /*.config(['$localStorageProvider',
            function ($localStorageProvider) {
                var mySerializer = function (value) {
                    // Do what you want with the value.
                    //angular.forEach(value, function(mvcIr, index){
                    //	value[index] = JSOG.encode(mvcIr);
                    //})
                    return value; //LZString.compress(JSON.stringify(JSOG.encode(value)));
                };

                var myDeserializer = function (value) {
                    return value;
                };

                $localStorageProvider.setSerializer(mySerializer);
                $localStorageProvider.setDeserializer(myDeserializer);
            }]);*/



    app1
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider', '$localStorageProvider'];
    function config($routeProvider, $localStorageProvider) {
        var mySerializer = function (value) {
            // Do what you want with the value.
            //angular.forEach(value, function(mvcIr, index){
            //	value[index] = JSOG.encode(mvcIr);
            //})
            return value; //LZString.compress(JSON.stringify(JSOG.encode(value)));
        };

        var myDeserializer = function (value) {
            return value;
        };

        $localStorageProvider.setSerializer(mySerializer);
        $localStorageProvider.setDeserializer(myDeserializer);

        $routeProvider
            .when('/', {
                controller: 'LoginController',
                templateUrl: 'view/login.html',
                controllerAs: 'vm'
            })

            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'view/login.html',
                controllerAs: 'vm'
            })
            .when('/logout', {
                controller: 'LogoutController',
                templateUrl: 'view/login.html',
                controllerAs: 'vm'
            })

            .when('/orders', {
                controller: 'OrdersController',
                templateUrl: 'view/orders.html',
                controllerAs: 'vm'
            })
            .when('/panel', {
            	controller: 'PanelController',
            	templateUrl: 'panel.html',
            	controllerAs: 'vm'
            })

            .otherwise({ redirectTo: '/login' });


    }

    run.$inject = ['$rootScope', '$http', '$location', 'UserService'];
    function run($rootScope, $http, $location, UserService) {
        $rootScope.checkLogin = function () {
            if (UserService.getCurrentUser() && UserService.getCurrentUser() !==null && UserService.getCurrentUser().username) {
                $rootScope.authenticated = true;
                $rootScope.loginButton = 'Logout';
            } else {
                $rootScope.authenticated = false;
                $rootScope.loginButton = 'Login';
            }
        };

        $rootScope.reauth = function (repeatFunction) {
            $http({
                method: 'POST',
                url: '/app-portal/api/login',
                data: {
                    'username': localStorage.getItem('username'),
                    'password': localStorage.getItem('password'),
                    'locale': 'en'
                },
                contentType: 'application/x-www-form-urlencoded'
            }).then(function (response) {
                localStorage.setItem('session_id', response.data);
                console.log('Session dead. Reauth');
                repeatFunction();
            }, function (response) {
                localStorage.removeItem('session_id');
                $location.path('/orders');
            });
        };

        $rootScope.checkLogin();
    }
    app1.directive('clickAndDisable', function() {
        return {
            scope: {
                clickAndDisable: '&'
            },
            link: function(scope, iElement, iAttrs) {
                iElement.bind('click', function() {
                    iElement.prop('disabled',true);
                    scope.clickAndDisable().finally(function() {
                        iElement.prop('disabled',false);
                    })
                });
            }
        };
    });
    app1.directive('myHtml', function ($compile) {

        return {
            restrict: 'EA',
            replace: true,
            scope: {
                compilex: '='

            },
            template:'<div>{{compilex}}</div>',
            link: function (scope, element, attrs) {
                scope.$watch(
                    function (scope) {
                        // watch the 'compile' expression for changes
                        return scope.$parent.$eval(attrs.compilex);
                    },
                    function (value) {
                        // when the 'compile' expression changes
                        // assign it into the current DOM
                        element.html(value);

                        // compile the new DOM and link it to the current
                        // scope.
                        // NOTE: we only compile .childNodes so that
                        // we don't get into infinite loop compiling ourselves
                        $compile(element.contents())(scope.$parent);
                    }
                );
            }
        };
    })


    app1.directive('ngIndustryCategories', function() {
        return {
            restrict: "E",
            scope: {family: '='},
            template:
            '<p>{{ family.name }}{{test }}</p>'+
            '<ul>' +
            '<li ng-repeat="child in family.children">' +
            '<ngIndustryCategories family="child"></ngIndustryCategories>' +
            '</li>' +
            '</ul>',
            compile: function(element) {
                return RecursionHelper.compile(element, function(scope, iElement, iAttrs, controller, transcludeFn){
                    // Define your normal link function here.
                    // Alternative: instead of passing a function,
                    // you can also pass an object with
                    // a 'pre'- and 'post'-link function.
                });
            }
        };
    });
    app1.directive('ngReplace', function() {
        return {
            restrict: 'A',
            link: function (scope, iElement, iAttrs) {

                var svg = angular.element(scope.message);
                iElement.append(svg);
            },
            template: '<div ><h4>Weather for {{message}}</h4></div>'
        }
    });
})();