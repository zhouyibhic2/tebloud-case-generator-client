

var app1 = angular
	.module('app');

app1.controller('PanelController', PanelController);
function PanelController($scope, $interval,  $timeout, $sce, $http, $localStorage, $q, $uibModal, usSpinnerService, $rootScope, $location, $route) {

	$scope.$storage = $localStorage;

	angular.element(document).ready(function () {
		$scope.update();
	});


	$scope.pingMarkerScript = function () {
		if (ate_marker_script_execution_ended) {

			if (typeof $scope.mvcTempData.injectTimer !== "undefined") {
				clearTimeout($scope.mvcTempData.injectTimer);
				delete $scope.mvcTempData.injectTimer;
			}
			$scope.stopSpin();
			if (typeof $scope.mvcTempData.purposeOfInject !== "undefined") {
				ate_global_page_context_Watch.trigger($scope.mvcTempData.purposeOfInject);
				delete $scope.mvcTempData.purposeOfInject;
			}
			else
				ate_global_page_context_Watch.trigger();
			if (typeof $scope.mvcTempData.pingerPromise !== "undefined") {
				$interval.cancel($scope.mvcTempData.pingerPromise);
				delete $scope.mvcTempData.pingerPromise;
			}

		} else {
			sendObjectToInspectedPage({action: "script", content: "v1.0/messageback-marker-execution-pinger.js"});
		}
	}

	$scope.update = function () {
		//$scope.fruits = {};
		$scope.$apply();
		// fruitsService.loadFruits(function(data){
		// $scope.fruits = data;
		// $scope.$apply();
		// });
	}
	$scope.homeAction = function () {
		if ($scope.authenticated) {
			$rootScope.checkLogin($rootScope);
			$route.reload();
		}
		else {
			$rootScope.checkLogin($rootScope);
			$location.path('/login');
		}

	};

	//constant
	//$scope.TCG_SERVER_IP = "localhost";
	$scope.TCG_SERVER_IP = "peidong-3442";
	$scope.TCG_SERVER_PORT = "9080";
	$scope.LIFE_PORTLET_API_PREFIX = "/tebloud-tcg-liferay-portlet/services";

	$scope.TCG_URL_PART1 = function() {
		var domainName = "*";
		if ($scope.mvcCurrentIResult.domainName)
			domainName = $scope.mvcCurrentIResult.domainName
		var tcName = "*";
		if ($scope.mvcCurrentIResult.testCaseName)
			tcName = $scope.mvcCurrentIResult.testCaseName;
		return $scope.LIFE_PORTLET_API_PREFIX  +'/'+ domainName+'/*/'+tcName;
	}


	/****************************************************************mvc model code put below *******************************************************/
	$scope.mvcIResults = [];
	$scope.mvcCurrentIResult = {};
	$scope.mvcUIOperationStatus = {
		pageNotSavedYet: true,
		newScreenModelCreated: false,
		previousScreenOverridedByCurrentScreen: false,
		nextScreenExists: false,
		spinneractive: false,
		lastScreenOfTestCase: false,
		disableInput: true,
		runningModeProperties: {runningMode: 'SINGLE_APP_TESTING', usecaseAPPTrainingEventName: '$set'}

	};
	$scope.mvcTempData = {
		allUitrsRefs: {},
		screenNames: {},
		allTestCaseNames: {},
		allTestSuiteNames:{},
		predictedScreenNames: {},
		allUserValues: [],
		allFieldNames: []
	};
	/***************************************************************mvc*******************************/




	$scope.startSpin = function () {
		if (!$scope.mvcUIOperationStatus.spinneractive) {
			usSpinnerService.spin('spinner-1');
			$scope.startcounter++;
		}
	};

	$scope.stopSpin = function () {
		if ($scope.mvcUIOperationStatus.spinneractive) {
			usSpinnerService.stop('spinner-1');
		}
	};

	$rootScope.$on('us-spinner:spin', function (event, key) {
		$scope.mvcUIOperationStatus.spinneractive = true;
	});

	$rootScope.$on('us-spinner:stop', function (event, key) {
		$scope.mvcUIOperationStatus.spinneractive = false;
	});

	$scope.injectProcessor = function (purposeOfInject) {
		$scope.startSpin();
		$scope.mvcTempData.purposeOfInject = purposeOfInject;
		sendObjectToInspectedPage({action: "script", content: "v1.0/messageback-jquery-existence.js"});
		$scope.mvcTempData.injectTimer = setTimeout(function () {
			//window.ate_global_page_context = {prop: "new value"};
			ate_global_page_context_Watch.trigger(purposeOfInject);
			delete $scope.mvcTempData.injectTimer;
			delete $scope.mvcTempData.purposeOfInject;
			if (typeof $scope.mvcTempData.pingerPromise !== "undefined") {
				$interval.cancel($scope.mvcTempData.pingerPromise);
				delete $scope.mvcTempData.pingerPromise;
			}
		}, 60000);
		$scope.mvcTempData.pingerPromise = $interval(function () {
			$scope.pingMarkerScript();
		}, 2000);
	}
	$scope.autoAddTriggeredElements = function () {
		ate_marker_script_execution_ended = false;
		$scope.injectProcessor("addTriggeredElements")
	}

	$scope.tebloudPostReq = function(tebloudApiUrl, postData) {
		return {
			method: 'POST',
			url: tebloudApiUrl,
			headers: {'Content-Type': 'application/json'},
			//data: ate_global_page_documents
			data: postData,
			params: {'s': localStorage.getItem('session_id')}
		}
	}
	$scope.prependNewTriggeredElements = function () {
		var req = {};
		if (!$scope.mvcUIOperationStatus.pageNotSavedYet) {


			req = $scope.tebloudPostReq(
				'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() +'/findNewTriggeredElements',
				{
					newDoms: ate_global_page_context.pages,
					previousDoms: $scope.mvcCurrentIResult.screenNode.sourcingDoms
				}
			)

		} else {
			alert("failure message: screen hasn't been saved yet. This operation not allowed.");
			return;
		}
		$http(req).success(function (data, status, headers, config) {
			data = JSOG.decode(data);
			angular.forEach(data, function (uitr) {
				uitr.inputMLHtmlCodeChangedByUI = true;
			})
			$scope.mvcTempData.allUitrsRefs["unPredicted"] = data;

		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});
	}

	var unbind = ate_global_page_context_Watch.watch(function (newVal, purposeOfInject) {
		$scope.stopSpin();
		if (typeof newVal == "undefined") {
			alert("timeout, failed js injection, please retry.");

		} else {
			$scope.mvcCurrentIResult.screenNode.url = newVal.screenUrl;
			$scope.mvcCurrentIResult.screenNode.screenWidth = newVal.screenWidth;
			$scope.mvcCurrentIResult.screenNode.topDocumentHeight = newVal.topDocumentHeight;
			$scope.mvcCurrentIResult.screenNode.topDocumentWidth = newVal.topDocumentWidth;
			$scope.mvcCurrentIResult.domainName = newVal.domain;
			$scope.mvcCurrentIResult.domainProtocol = newVal.domainProtocol;
			$scope.mvcCurrentIResult.domainPort = newVal.domainPort;
			//$scope.page_context = ate_global_page_context;
			//////////////////////////////////////////////////////////


			/////////////////////////////////

			if (typeof purposeOfInject == "undefined") {
				//$scope.update();
			}
			else if (purposeOfInject === "addTriggeredElements") {
				$scope.prependNewTriggeredElements();
			}
		}
	});


	$scope.duplicatedUserValue = function (userValues, userValue) {
		for (var i = 0; i < userValues.length; i++) {
			if (userValues[i].value === userValue.value)
				return i;
		}
		return -1
	}
	$scope.changeInputMLHtmlCodeFlag = function (ikey, uitrIndex) {
		var uitrIndex = $scope.calculateAbsoluteIndex(ikey, uitrIndex);
		$scope.mvcTempData.allUitrsRefs[ikey][uitrIndex].inputMLHtmlCodeChangedByUI = true;
		$scope.mvcTempData.allUitrsRefs[ikey][uitrIndex].id = null;
		$scope.mvcTempData.allUitrsRefs[ikey][uitrIndex].graphId = null;
		$scope.mvcTempData.allUitrsRefs[ikey][uitrIndex].testcases = [];
	}
	$scope.parseInputGuid = function (coreElementHtmlCode) {
		var elements = $("<div>" + coreElementHtmlCode + "</div>");
		var inputs = elements.find("input");
		var selects = elements.find("select");
		var textareas = elements.find("textarea");
		var guid = "";
		if (inputs.length > 0) {
			guid = inputs.get(0).getAttribute("ate-guid");
		} else if (selects.length > 0) {
			guid = selects.get(0).getAttribute("ate-guid");
		} else if (textareas.length > 0) {
			guid = textareas.get(0).getAttribute("ate-guid");
		}
		return guid;
	}
	$scope.tmpUserValue = function (selected, row) {
		if (typeof selected !== "undefined") {
			$scope.setUitrUserValue(selected.title, this.$parent.$parent.ikey, this.$parent.$index, false);
			if ($scope.mvcTempData.allUserValues.map(function(fn){
					return fn.value;
				}).indexOf(selected.title)==-1) {
				$scope.mvcTempData.allUserValues.push({value: selected.title, name: selected.title, desc: ""})

			}
			var fieldName = $scope.mvcTempData.allUitrsRefsPagingView[this.$parent.$parent.ikey][this.$parent.$index].pioPredictLabelResult.value;
			angular.forEach($scope.mvcTempData.allFieldNames, function(fname) {
				if (fname.value == fieldName) {
					fname.desc = selected.title;
				}
			})
		} else {
			$scope.setUitrUserValue("", this.$parent.$parent.ikey, this.$parent.$index, false);
		}




	}
	$scope.tmpScreenName = function (selected) {
		if (typeof selected !== "undefined") {
			$scope.mvcCurrentIResult.screenNode.name = selected.title;

		}




	}

	$scope.tmpTestCaseFunc = function (selected) {
		if (typeof selected !== "undefined") {
			$scope.mvcCurrentIResult.testCaseName = selected.title;

		}


	}
	$scope.tmpTestSuiteFunc = function (selected) {
		if (typeof selected !== "undefined") {
			$scope.mvcCurrentIResult.testSuitesMap[0].name = selected.title;

		}
	}
	$scope.tmpSubTestSuiteFunc = function (selected) {
		if (typeof selected !== "undefined") {
			$scope.mvcCurrentIResult.testSuitesMap[1].name = selected.title;

		}
	}

	$scope.setUitrUserValue = function (newValue, key, index, broadCastUserValue) {
		var relativeIndex = index;
		index = $scope.calculateAbsoluteIndex(key, index);
		if (newValue) {

			if ($scope.mvcTempData.allUitrsRefs[key][index].userValues.length > 0) {
				$scope.mvcTempData.allUitrsRefs[key][index].userValues[0].name = newValue;
				$scope.mvcTempData.allUitrsRefs[key][index].userValues[0].value = newValue;
			} else {
				var tmpObj = {name: newValue, value: newValue};
				$scope.mvcTempData.allUitrsRefs[key][index].userValues.push(tmpObj);
			}

			$scope.sync_page_element(newValue, $scope.parseInputGuid($scope.mvcTempData.allUitrsRefs[key][index].elementCoreHtmlCode))
			if (broadCastUserValue)
				$scope.$broadcast('angucomplete-alt:changeInput', "userInuptValue-" + key + "-" + relativeIndex, $scope.mvcTempData.allUitrsRefs[key][index].userValues[0].value);

		} else {
			if ($scope.mvcTempData.allUitrsRefs[key][index].userValues && $scope.mvcTempData.allUitrsRefs[key][index].userValues.length > 0) {
				if (typeof $scope.mvcTempData.allUitrsRefs[key][index].userValues[0].graphId !== "undefined" && $scope.mvcTempData.allUitrsRefs[key][index].userValues[0].graphId !== null)
					$scope.deleteUitrUserValue($scope.mvcTempData.allUitrsRefs[key][index].userValues[0].graphId, $scope.mvcTempData.allUitrsRefs[key][index].graphId);
			}

			$scope.mvcTempData.allUitrsRefs[key][index].userValues = [];
			if (broadCastUserValue)
				$scope.$broadcast('angucomplete-alt:clearInput', "userInuptValue-" + key + "-" + relativeIndex);
		}
	}



	$scope.loadScreen = function(){
		var screenNodeId = $scope.mvcTempData.loadScreenGraphId;
		$scope.mvcGetIResult(screenNodeId, $scope.mvcCurrentIResult.testCaseName);


	}
	$scope.tmpPioPredictLabelResult = function (selected) {
		if (typeof selected !== "undefined") {
			var relativeIndex = this.$parent.$index;
			var absoluteIndex = $scope.calculateAbsoluteIndex(this.$parent.$parent.ikey, relativeIndex);
			if (typeof $scope.mvcTempData.allUitrsRefs[this.$parent.$parent.ikey][absoluteIndex].pioPredictLabelResult !== "undefined") {
				$scope.mvcTempData.allUitrsRefs[this.$parent.$parent.ikey][absoluteIndex].pioPredictLabelResult.name = selected.title;
				$scope.mvcTempData.allUitrsRefs[this.$parent.$parent.ikey][absoluteIndex].pioPredictLabelResult.value = selected.title;
				$scope.mvcTempData.allUitrsRefs[this.$parent.$parent.ikey][absoluteIndex].inputLabelName = selected.title;
				$scope.setUitrUserValue(selected.originalObject.desc, this.$parent.$parent.ikey, relativeIndex, true);
				if ($scope.mvcTempData.allFieldNames.map(function(fn){
						return fn.value;
					}).indexOf(selected.title)==-1)
					$scope.mvcTempData.allFieldNames.push({value:selected.title, name:selected.title, desc: ""})
				/*if (selected.originalObject.desc) {
				 if ($scope.mvcTempData.allUitrsRefs[this.$parent.$parent.ikey][this.$parent.$index].userValues.length == 0) {
				 $scope.mvcTempData.allUitrsRefs[this.$parent.$parent.ikey][this.$parent.$index].userValues[0] = {
				 name: selected.originalObject.desc,
				 value: selected.originalObject.desc
				 }
				 } else {
				 $scope.mvcTempData.allUitrsRefs[this.$parent.$parent.ikey][this.$parent.$index].userValues[0].value = selected.originalObject.desc;
				 $scope.mvcTempData.allUitrsRefs[this.$parent.$parent.ikey][this.$parent.$index].userValues[0].name = selected.originalObject.desc;
				 }
				 $scope.sync_page_element(selected.originalObject.desc, $scope.parseInputGuid($scope.mvcTempData.allUitrsRefs[this.$parent.$parent.ikey][this.$parent.$index].elementCoreHtmlCode));
				 if ($scope.mvcTempData.allUitrsRefs[this.$parent.$parent.ikey][this.$parent.$index].userValues.length > 0)
				 $scope.$broadcast('angucomplete-alt:changeInput', "userInuptValue-" + this.$parent.$parent.ikey + "-" + this.$parent.$index, $scope.mvcTempData.allUitrsRefs[this.$parent.$parent.ikey][this.$parent.$index].userValues[0].value);
				 else
				 $scope.$broadcast('angucomplete-alt:clearInput', "userInuptValue-" + this.$parent.$parent.ikey + "-" + this.$parent.$index);
				 } else {
				 $scope.$broadcast('angucomplete-alt:clearInput', "userInuptValue-" + this.$parent.$parent.ikey + "-" + this.$parent.$index);
				 }*/

			} else {
				//$scope.fruits[this.$parent.$index].pioPredictLabelResult = {name: selected.title, value: selected.title}
			}
		}
	}

	$scope.screenTypeSwitcher = function () {
		switch ($scope.mvcCurrentIResult.screenNode.screenType) {
			case "WINDOWFILEPICKER":
///////TOOD
				if (typeof $scope.$storage.lastScreenNode != null && typeof $scope.$storage.lastScreenNode != "undefined") {
					$scope.fruits = [];

					$scope.fruits[0] = {
						inputLabelName: "FileDialogOKButton",
						inputMLHtmlCode: "FileDialogOKButton",
						userInputType: "SCREENJUMPER",
						actionTrigger: true,
						belongToCurrentTestCase: true,
						pioPredictLabelResult: {value: "FileDialogOKButton", name: "FileDialogOKButton"},
						userValues: [{value: "", name: ""}]
					};
					$scope.countrySelected14 = "WindowsFilePicker";
					$scope.page_context = {domainProtocol: "windows:", domainPort: "", screenUrl: "filePickerDialog"};
					$scope.domainName = "filePickerDialog";
				} else {

					alert("there is no element  to trigger the file picker in previous screen.")
				}
				//$scope.$apply();
				break;
			default:
				break;
		}
	}


	$scope.openModalOfScreenTrainingWords = function (size, parentSelector) {
		var parentElem = parentSelector ?
			angular.element($document[0].querySelector('.ng-scope' + parentSelector)) : undefined;
		var modalInstance = $uibModal.open({
			//animation: $ctrl.animationsEnabled,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'myModalScreenTrainingWords.html',
			controller: 'ModalInstanceCtrl',
			controllerAs: '$ctrl',
			size: size,
			appendTo: parentElem,
			resolve: {
				screenTrainingWords: function () {
					return $scope.mvcCurrentIResult.screenNode.inputMLHtmlWords;
				}
			}
		});


	};

	$scope.screenPredictStrategySwitcher = function () {
		if ($scope.mvcTempData.predictedScreenNames != null && typeof $scope.mvcTempData.predictedScreenNames != "undefined") {
			$scope.mvcCurrentIResult.screenNode.name = Object.keys($scope.mvcTempData.predictedScreenNames[$scope.mvcCurrentIResult.screenNode.screenPredictStrategy])[0];
			$scope.mvcCurrentIResult.screenNode.inputMLHtmlWords = $scope.mvcTempData.predictedScreenNames[$scope.mvcCurrentIResult.screenNode.screenPredictStrategy][$scope.mvcCurrentIResult.screenNode.name].screenTrainingWords;
			$scope.updateScreenNameAutoComplete();
			//$("#ex14").find("input")[0].value = $scope.mvcCurrentIResult.screenNode.name;
		}
	}
	// Unbind the listener when the scope is destroyed
	$scope.$on('$destroy', unbind);

	$scope.getAllValues = function (mapVal) {
		return Object.keys(mapVal).map(function (key) {
			return mapVal[key];
		});
	}
	$scope.addWebElement = function () {
		var allUitrsIn2DArr = $scope.getAllValues($scope.mvcTempData.allUitrsRefs);

		var tempArr = [].concat.apply([], allUitrsIn2DArr);
		if (tempArr.length > 0) {
			var tmpArr = tempArr.slice(0, 1);
			var json = angular.toJson(JSOG.encode(tmpArr[0]));
			var newUitr = JSOG.decode(JSON.parse(json));
			if (typeof newUitr["@uuid"] != "undefined")
				delete newUitr["@uuid"];
			if (typeof newUitr["@id"] != "undefined")
				delete newUitr["@id"];
			if (typeof newUitr["@ref"] != "undefined")
				delete newUitr["@ref"];
			if (typeof newUitr["__jsogObjectId"] != "undefined")
				delete newUitr["__jsogObjectId"];
			if (typeof newUitr["__jsogObjectRef"] != "undefined")
				delete newUitr["__jsogObjectRef"];
			newUitr.inputMLHtmlCode = "";
			newUitr.inputLabelName = "";
			newUitr.identifierString = "";
			newUitr.inputMLHtmlWords = "";
			newUitr.comparableHtmlCode = "";
			newUitr.comparableLabelName = "";
			newUitr.id = "";

			newUitr.elementCoreHtmlCodeWithoutGuidValue = "";
			newUitr.trainedResult = "";
			newUitr.pioPredictLabelResult = {};
			newUitr.pioPredictConfidence = 0;
			newUitr.userValues = [];
			newUitr.testcases = [];
			newUitr.computedCssSizes = [];
			newUitr.inputMLHtmlCodeChangedByUI = false;
			newUitr.triggeredBy = null;
			newUitr.graphId = null;
			newUitr.actionTrigger = false;
			newUitr.elementCoreHtmlCode = "";
			newUitr.userInputType = "unPredicted"
			newUitr.alternativeUserInputTypes = [];
			newUitr.screenUitrProbability = 0;
			if (typeof $scope.mvcTempData.allUitrsRefs["unPredicted"] === "undefined")
				$scope.mvcTempData.allUitrsRefs["unPredicted"] = [];
			$scope.mvcTempData.allUitrsRefs["unPredicted"].splice(0, 0, newUitr);
		} else {
			//alert("abc")
			if (typeof $scope.mvcTempData.allUitrsRefs["unPredicted"] === "undefined")
				$scope.mvcTempData.allUitrsRefs["unPredicted"] = [];
			$scope.mvcTempData.allUitrsRefs["unPredicted"].splice(0, 0, {inputLabelName: "", inputMLHtmlCode: ""});
		}
	}
	$scope.removeRow = function (ikey, fruit) {
		$scope.mvcTempData.allUitrsRefs[ikey].splice($scope.mvcTempData.allUitrsRefs[ikey].indexOf(fruit), 1);
		$scope.updateAngucompleteRows(ikey);
	}

	$scope.trustAsHtml = $sce.trustAsHtml;

	$scope.ate_sync_value = function (fruit, key, index, guid) {
		var str = "userInuptValue-" + key + "-" + index;
		//$(str).find("input").get(0).value = fruit.userValues[0].value;
		$scope.$broadcast('angucomplete-alt:changeInput', str, fruit.userValues[0].value);
		$scope.sync_page_element(fruit.userValues[0].value, guid)

	}
	/*below dummy code for execute value sync for page element, please don't delete*/
	/*
	 function ate___getElementByGuid(doc, guid) {
	 var arr_elms = [];
	 arr_elms = doc.body.getElementsByTagName("*");
	 var elms_len = arr_elms.length;

	 for (var i = 0; i < elms_len; i++) {
	 if(arr_elms[i].getAttribute("ate-guid") !== null && arr_elms[i].getAttribute("ate-guid")==guid){
	 return arr_elms[i];
	 }
	 }
	 return null;
	 }

	 function ate___get_body_equavelent_element(doc) {
	 if (typeof doc.body != "undefined") {
	 //document.body either return frameset or body element in chrome
	 return doc.body;
	 }
	 else if (typeof doc.getElementsByTagName("frameset") != "undefined") {
	 return doc.getElementsByTagName("frameset")[0];
	 } else {
	 return doc.getElementsByTagName("body")[0];
	 }
	 }

	 function ate___getElementInIFrames(douc, guid) {
	 var iframes = ate___get_body_equavelent_element(douc).getElementsByTagName("iframe");
	 for (var index=0; index<iframes.length; index++) {
	 var target = ate___getElementByGuid(iframes[index].contentWindow.document, guid);
	 if (target!==null) {
	 return target;
	 }
	 debugger;
	 //console.log("hello stackoverflow");
	 var targetInFrame = ate___getElementInIFrames(iframes[index].contentWindow.document, guid);
	 if (targetInFrame!==null) {
	 return targetInFrame;
	 }
	 }
	 var frames = ate___get_body_equavelent_element(douc).getElementsByTagName("frame");
	 for (var index=0; index<frames.length; index++) {
	 var target = ate___getElementByGuid(frames[index].contentWindow.document, guid);
	 if (target!==null) {
	 return target;
	 }
	 debugger;
	 var targetInFrame =  ate___getElementInIFrames(frames[index].contentWindow.document, guid);
	 if (targetInFrame!==null) {
	 return targetInFrame;
	 }
	 }
	 return null;
	 }

	 var ate___guid = "a8963eed-e6cd-4967-bd7b-0b94d1201b15";
	 var ate___value = "abc";

	 var ate___targetElement = ate___getElementByGuid(document, ate___guid);

	 if (ate___targetElement===null) {
	 ate___targetElement = ate___getElementInIFrames(document, ate___guid);
	 }


	 if (ate___targetElement!==null) {
	 if (ate___targetElement.nodeName.toUpperCase()=="INPUT" || ate___targetElement.nodeName.toUpperCase()=="TEXTAREA") {
	 if(ate___targetElement.getAttribute("type") && ate___targetElement.getAttribute("type").toUpperCase()!="BUTTON" && ate___targetElement.getAttribute("type").toUpperCase()!="SUBMIT")
	 ate___targetElement.value=ate___value;
	 }
	 else if (ate___targetElement.nodeName.toUpperCase()=="SELECT") {
	 for (var ate___indexx=0; ate___indexx<ate___targetElement.options.length; ate___indexx++) {
	 if(ate___targetElement.options[ate___indexx].text.trim() == ate___value.trim())
	 ate___targetElement.options[ate___indexx].selected= true;
	 }
	 }
	 }
	 */
	/*above dummy code*/

	$scope.sync_page_element = function (value, guid) {
		var exec_str = "function ate___getElementByGuid(doc, guid) {\r\n\t\t\tvar arr_elms = [];\r\n\t\t\tarr_elms = doc.body.getElementsByTagName(\"*\");\r\n\t\t\tvar elms_len = arr_elms.length;\r\n\r\n\t\t\tfor (var i = 0; i < elms_len; i++) {\r\n\t\t\t\tif(arr_elms[i].getAttribute(\"ate-guid\") !== null && arr_elms[i].getAttribute(\"ate-guid\")==guid){\r\n\t\t\t\t\treturn arr_elms[i];\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t\treturn null;\r\n\t\t}\r\n\r\n\tfunction ate___get_body_equavelent_element(doc) {\r\n\t\tif (typeof doc.body != \"undefined\") {\r\n\t\t\t\/\/document.body either return frameset or body element in chrome\r\n\t\t\treturn doc.body;\r\n\t\t}\r\n\t\telse if (typeof doc.getElementsByTagName(\"frameset\") != \"undefined\") {\r\n\t\t\treturn doc.getElementsByTagName(\"frameset\")[0];\r\n\t\t} else {\r\n\t\t\treturn doc.getElementsByTagName(\"body\")[0];\r\n\t\t}\r\n\t}\r\n\r\n\t\tfunction ate___getElementInIFrames(douc, guid) {\r\n\t\t\tvar iframes = ate___get_body_equavelent_element(douc).getElementsByTagName(\"iframe\");\r\n\t\t\tfor (var index=0; index<iframes.length; index++) {\r\n\t\t\t\tvar target = ate___getElementByGuid(iframes[index].contentWindow.document, guid);\r\n\t\t\t\tif (target!==null) {\r\n\t\t\t\t\treturn target;\r\n\t\t\t\t}\r\n\t\t\t\r\n\t\t\t\t\/\/console.log(\"hello stackoverflow\");\r\n\t\t\t\tvar targetInFrame = ate___getElementInIFrames(iframes[index].contentWindow.document, guid);\r\n\t\t\t\tif (targetInFrame!==null) {\r\n\t\t\t\t\treturn targetInFrame;\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t\tvar frames = ate___get_body_equavelent_element(douc).getElementsByTagName(\"frame\");\r\n\t\t\tfor (var index=0; index<frames.length; index++) {\r\n\t\t\t\tvar target = ate___getElementByGuid(frames[index].contentWindow.document, guid);\r\n\t\t\t\tif (target!==null) {\r\n\t\t\t\t\treturn target;\r\n\t\t\t\t}\r\n\t\t\t\t\r\n\t\t\t\tvar targetInFrame =  ate___getElementInIFrames(frames[index].contentWindow.document, guid);\r\n\t\t\t\tif (targetInFrame!==null) {\r\n\t\t\t\t\treturn targetInFrame;\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t\treturn null;\r\n\t\t}\r\n\r\n\t\tvar ate___guid = \"" + guid + "\";\r\n\t\tvar ate___value = \"" + value + "\";\r\n\r\n\t\tvar ate___targetElement = ate___getElementByGuid(document, ate___guid);\r\n\r\n\t\tif (ate___targetElement===null) {\r\n\t\t\tate___targetElement = ate___getElementInIFrames(document, ate___guid);\r\n\t\t}\r\n\r\n\r\n\t\tif (ate___targetElement!==null) {\r\n\t\t\tif (ate___targetElement.nodeName.toUpperCase()==\"INPUT\" || ate___targetElement.nodeName.toUpperCase()==\"TEXTAREA\") {\r\n\t\t\t\tif(ate___targetElement.getAttribute(\"type\") && ate___targetElement.getAttribute(\"type\").toUpperCase()!=\"BUTTON\" && ate___targetElement.getAttribute(\"type\").toUpperCase()!=\"SUBMIT\")\r\n\t\t\t\t\tate___targetElement.value=ate___value;\r\n\t\t\t}\r\n\t\t\telse if (ate___targetElement.nodeName.toUpperCase()==\"SELECT\") {\r\n\t\t\t\tfor (var ate___indexx=0; ate___indexx<ate___targetElement.options.length; ate___indexx++) {\r\n\t\t\t\t\tif(ate___targetElement.options[ate___indexx].text.trim() == ate___value.trim())\r\n\t\t\t\t\t\tate___targetElement.options[ate___indexx].selected= true;\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t}"
		//var exec_str = "function ate___getElementByGuid(doc, guid) {\r\nvar arr_elms = [];\r\narr_elms = doc.body.getElementsByTagName(\"*\");\r\nvar elms_len = arr_elms.length;\r\n\r\nfor (var i = 0; i < elms_len; i++) {\r\n  if(arr_elms[i].getAttribute(\"ate-guid\") !== null && arr_elms[i].getAttribute(\"ate-guid\")==guid){  \r\n     return arr_elms[i];\r\n   }\r\n}\r\nreturn null;\r\n}\r\n\r\n\r\n\r\nfunction ate___getElementInIFrames(douc, guid) {\r\n\tvar frames = douc.body.getElementsByTagName(\"iframe\");\r\n\tfor (var index=0; index<frames.length; index++) {\r\n\t\tvar target = ate___getElementByGuid(frames[index].contentWindow.document, guid);\r\n\t\tif (target!==null) {\r\n\t\t\treturn target;\r\n\t\t}\r\n\t\treturn ate___getElementInIFrames(frames[index].contentWindow.document, guid); \r\n\t}\r\n\treturn null;\r\n}\r\n\r\nvar ate___guid = \""+guid+"\";\r\nvar ate___value = \""+value+"\";\r\n\r\nvar ate___targetElement = ate___getElementByGuid(document, ate___guid);\r\n\r\nif (ate___targetElement===null) {\r\n\tate___targetElement = ate___getElementInIFrames(document, ate___guid);\r\n}\r\n\r\n\r\nif (ate___targetElement!==null) {\r\n\tif (ate___targetElement.nodeName==\"INPUT\" || ate___targetElement.nodeName==\"TEXTAREA\") \r\n\t\tate___targetElement.value=ate___value;\r\n\telse if (ate___targetElement.nodeName==\"SELECT\") {\r\n\t\tfor (var ate___indexx=0; ate___indexx<ate___targetElement.options.length; ate___indexx++) {\r\n\t\t\tif(ate___targetElement.options[ate___indexx].text.trim() == ate___value.trim())\r\n\t\t\t\tate___targetElement.options[ate___indexx].selected= true;\r\n\t\t}\r\n\t}\r\n}\r\n"
		sendObjectToInspectedPage({action: "code", content: exec_str});
	}
	$scope.ate_sync_select_value = function (fruit, key, index, guid) {
		var optionValue = fruit.userValues[0].value;
		var str = "userInuptValue-" + key + "-" + index;

		//$(str).find("input").get(0).value = $('option[value="'+optionValue+'"]').get(0).text;
		var newValue = $('option[value="' + optionValue + '"]').get(0).text;
		$scope.$broadcast('angucomplete-alt:changeInput', str, newValue);
		fruit.userValues[0].value = newValue//$('option[value="'+optionValue+'"]').get(0).text;
		fruit.userValues[0].name = newValue//$('option[value="'+optionValue+'"]').get(0).text;

		$scope.sync_page_element(fruit.userValues[0].value, guid);
	}
	$scope.removeStyleInInputMLHtmlCode = function (inputMLHtmlCode) {


		var retVal = inputMLHtmlCode.replace(/style=(".*?"|'.*?'|[^"'][^\s]*)/gim, "");
		var elements = $("<div>" + inputMLHtmlCode + "</div>");
		elements.find("*").each(function (ind, elm) {
			var attNamesToRemove = [];
			for (var i = 0, atts = elm.attributes, n = atts.length; i < n; i++) {
				if (atts[i].nodeName.indexOf("ng-") == 0)
					attNamesToRemove.push(atts[i].nodeName);
			}
			for (var i = 0; i < attNamesToRemove.length; i++) {
				elm.removeAttribute(attNamesToRemove[i]);
			}

		})
		elements.find("input").each(function (ind, elm) {

			elm.setAttribute("ng-model", "fruit.userValues[0].value")
			elm.setAttribute("ng-change", "ate_sync_value(fruit, $parent.ikey, $index, '" + elm.getAttribute("ate-guid") + "')")
		})
		elements.find("select").each(function (ind, elm) {

			elm.setAttribute("ng-model", "fruit.userValues[0].value")
			elm.setAttribute("ng-change", "ate_sync_select_value(fruit, $parent.ikey, $index, '" + elm.getAttribute("ate-guid") + "')")
		})
		return elements[0].innerHTML;


	}

	$scope.preprocessing = function () {
		if (!$scope.verifyTestSuiteAndTestCaseName())  return $q.reject();
		var req = {};
		var currentIResultIndex = $scope.mvcIResults.indexOf($scope.mvcCurrentIResult);
		if (currentIResultIndex > 0) {
			var previousIResult = $scope.mvcIResults[currentIResultIndex - 1];
			req = $scope.tebloudPostReq(
				 'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() +'/preprocessing',
				 {newDoms: ate_global_page_context.pages, previousDoms: previousIResult.screenNode.sourcingDoms}
			)
		} else {
			req = $scope.tebloudPostReq(
				'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() +'/preprocessing',
				{newDoms: ate_global_page_context.pages, previousDoms: []}
			)
		}
		$http(req).success(function (data, status, headers, config) {
			data = JSOG.decode(data);
			$scope.mvcTempData.allUitrsRefs["unPredicted"] = data;
			angular.forEach($scope.mvcTempData.allUitrsRefs["unPredicted"], function (uitr) {
				uitr.inputMLHtmlCodeChangedByUI = true;
				uitr.userInputType = "unPredicted";
			});
			$scope.pagePredict();
		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});

	};

	//$scope.pioResult2Npl = function(index) {
	//	$scope.fruits[index].inputLabelName = $scope.fruits[index].pioPredictLabelResult.value;
	//}
	//$scope.nplResult2Pio = function(index) {
	//	$scope.fruits[index].pioPredictLabelResult.value = $scope.fruits[index].inputLabelName;
	//	$("#pioPredictLabelResult" + index).find("input").get(0).value = ($scope.fruits[index].inputLabelName);
	//}


	$scope.startNewRootScreenNode = function () {
		$localStorage.$reset();
		$scope.$storage.mvcIResults = LZString.compress(JSON.stringify(JSOG.encode([])));
		$scope.mvcUIOperationStatus.newScreenModelCreated = false;
		//TODO refresh all the graph ids
		alert("next screen will be saved as independent node. Please don't forget fresh this page to clean up the graph ids.");
		//$scope.update();
	}
//////////////////////////////////////////////
	$scope.saveIntermediateResultForWindowsFilePicker = function (samePageUpdate) {
		var tmpScreenName;
		if (typeof $scope.countrySelected14 === 'undefined') {
			alert("no screen name set yet!");
			return $q.reject();

		}
		if (typeof $scope.countrySelected14.originalObject !== 'undefined') tmpScreenName = $scope.countrySelected14.originalObject.name;
		else tmpScreenName = $scope.countrySelected14;
		ate_global_page_context = {pages: []};
		for (var ind = 0; ind < $scope.fruits.length; ind++) {
			if ($scope.fruits[ind].userInputType === "SCREENJUMPER") {
				if ($scope.fruits[ind].inputLabelName !== "FileDialogOKButton") {
					alert("WindowsFilePicker Screen should have FileDialogOKButton screen jumper.")
					return $q.reject();
				}
			}
			if ($scope.fruits[ind].userInputType === "SCREENJUMPER") {
				if ($scope.fruits[ind].userValues.length === 0) {
					alert("please give a value for the file picker dialog. For example, if this is a file picker screen, please give the file name with its path. ")
					return $q.reject();
				}

			}
		}
		//if (inScreenJumperUitrs.length != 1) alert("this windows file picker screen should have been triggered by only one inScreenJumper");
		$scope.saveIntermediateResult(samePageUpdate);

	}
////////////////////////////////////////////////

	$scope.verifyRegularScreenInputs = function (updateOperation) {
		if (typeof $scope.mvcCurrentIResult.screenNode.name === 'undefined') {
			alert("no screen name set yet!");
			return false;

		}

		var screenJumperActionTrigger = $scope.mvcTempData.allUitrsRefs["SCREENJUMPER"].filter(function (uitr) {
			return uitr.actionTrigger && uitr.belongToCurrentTestCase
		});
		var screenElementChangerActionTrigger = $scope.mvcTempData.allUitrsRefs["SCREENCHANGEUITR"].filter(function (uitr) {
			return uitr.actionTrigger && uitr.belongToCurrentTestCase
		});
		var inScreenJumperActionTrigger = $scope.mvcTempData.allUitrsRefs["INSCREENJUMPER"].filter(function (uitr) {
			return uitr.actionTrigger && uitr.belongToCurrentTestCase
		});
		var allUitrsIn2DArr = $scope.getAllValues($scope.mvcTempData.allUitrsRefs);
		var tempArr = [].concat.apply([], allUitrsIn2DArr);
		for (var ind = 0; ind < tempArr.length; ind++) {


			if (tempArr[ind].userInputType == "USERINPUT" && tempArr[ind].userValues.length == 0) {
				alert("user input: '" + ind + " " + tempArr[ind].pioPredictLabelResult.value + "' has no user value");
				return false;
			}
			if (tempArr[ind].userInputType == "SCREENCHANGEUITR" && tempArr[ind].userValues.length == 0) {
				var screenChangeUitrQ = confirm("screen change uitr '" + ind + " " + tempArr[ind].pioPredictLabelResult.value + "' has no user input value, continue?", "yes", "no");
				if (!screenChangeUitrQ)
					return false;

			}
			if (tempArr[ind].userInputType == "SCREENCHANGEUITR" && tempArr[ind].actionTrigger == false && tempArr[ind].belongToCurrentTestCase) {
				var screenChangeUitrQ = confirm("SCREEN CHANGE UITR '" + ind + " " + tempArr[ind].pioPredictLabelResult.value + "' is not action trigger, continue?", "yes", "no");
				if (!screenChangeUitrQ)
					return false;

			}

			if (tempArr[ind].userInputType == "SCREENJUMPER" && tempArr[ind].userValues.length > 0) {
				var screenChangeUitrQ = confirm("SCREEN JUMPER: '" + ind + " " + tempArr[ind].pioPredictLabelResult.value + "' has user value, continue?");
				if (!screenChangeUitrQ)
					return false;
			}

		}

		var currentIResultIndex = $scope.mvcIResults.indexOf($scope.mvcCurrentIResult);
		if (currentIResultIndex > 0 && !updateOperation && $scope.mvcIResults[currentIResultIndex - 1].screenNode.name == $scope.parseScreenName()) {
			var yesgo = confirm("screen name is same as the previous screen name?");
			if (!yesgo)
				return false;
		}

		if ($scope.mvcCurrentIResult.screenTriggeredByPreviousScreenUitrId == 0 && currentIResultIndex > 0 && typeof $scope.mvcIResults[currentIResultIndex - 1].screenNode.graphId !== "undefined" && $scope.mvcIResults[currentIResultIndex - 1].screenNode.graphId !== null) {
			alert("please select screen step out trigger uitr");
			return false;
		}
		if (screenJumperActionTrigger.length === 0 && screenElementChangerActionTrigger.length === 0 && inScreenJumperActionTrigger.length == 0) {
			var r = confirm("There is no action trigger element collected. Are you doing last screen element collection?", "yes", "no");
			if (r === false) {
				return false;
			} else {
				$scope.mvcUIOperationStatus.lastScreenOfTestCase = true;
			}
		}

		if (screenJumperActionTrigger.length > 1) {
			alert("there are more than 1 screen jumper action triggers");
			return false;

		} else if (screenJumperActionTrigger.length == 1) {

			if (screenJumperActionTrigger[0].userInputType === "INSCREENJUMPER") {

				alert("wrong userinputtype set for screen jumper")
				return false;
			}

			if (screenJumperActionTrigger[0].userInputType === "SCREENJUMPER") {
				var r2 = confirm("Is it a screen jump action? Note: WindowsFilePicker is considered as element change uitr instead of screenJumper", "yes", "no");
				if (r2)
					$scope.mvcUIOperationStatus.lastScreenOfTestCase = false;
				else
					return false;
			}
			if (screenJumperActionTrigger[0].userInputType === "USERINPUT") {
				var r3 = confirm("Are you sure this user input is a screen jumper?", "yes", "no");
				if (r3)
					$scope.mvcCurrentIResult.inScreenJump = true;
				else
					return false;
			}

		}
		var r10 = confirm("Confirm the screen name is correct?  ====" + $scope.parseScreenName() + "====", "yes", "no");
		if (!r10)
			return false;
		if ($scope.mvcCurrentIResult.testCaseName == null || typeof $scope.mvcCurrentIResult.testCaseName === "undefined") {
			alert("testcase name is empty");
			return false;
		}
		return true;

	}

	$scope.verifyTestSuiteAndTestCaseName = function () {
		if (!$scope.mvcCurrentIResult.testSuitesMap[0].name) {
			alert("no test suite name yet!");
			return false;

		}
		if (!$scope.mvcCurrentIResult.testSuitesMap[1].name) {
			alert("no Sub test suite name yet!");
			return false;

		}
		if (!$scope.mvcCurrentIResult.testCaseName) {
			alert("no test case name yet!");
			return false;

		}
		return true;

	}

	$scope.getGraphNodeByGraphId = function (graphId) {
		var allUitrsIn2DArr = $scope.getAllValues($scope.mvcTempData.allUitrsRefs);
		var tempArr = [].concat.apply([], allUitrsIn2DArr);
		for (var ind = 0; ind < tempArr.length; ind++) {
			if (tempArr[ind].graphId == graphId) {
				return tempArr[ind];
			}
		}
	}

	$scope.sync_tempDataUitrs_to_screenNode = function () {

		angular.forEach($scope.mvcTempData.allUitrsRefs, function (uitrs, key) {
			if (key == "SCREENJUMPER")
				$scope.mvcCurrentIResult.screenNode.actionUitrs = uitrs;
			if (key == "INSCREENJUMPER")
				$scope.mvcCurrentIResult.screenNode.clickUitrs = uitrs;
			if (key == "SCREENCHANGEUITR")
				$scope.mvcCurrentIResult.screenNode.screenElementChangeUitrs = uitrs;
			if (key == "USERINPUT")
				$scope.mvcCurrentIResult.screenNode.userInputUitrs = uitrs;
		})
	}
	$scope.sync_tempDataUitrs_to_view = function () {

		if (typeof $scope.mvcTempData.uitrPagingVars === "undefined")
			$scope.resetUitrsPagingVars(0);
		else
			$scope.resetUitrsPagingVars($scope.mvcTempData.uitrPagingVars.currentPage);
		$scope.updateUitrsPagingView();
	}
	$scope.sync_screenNode_to_tempDataUitrs = function () {

		$scope.initMvcTempData();
		$scope.updateAllAngucompleteRows();
	}
	$scope.$watch('mvcTempData.allUitrsRefs', $scope.sync_tempDataUitrs_to_view, true);
	//$scope.$watch('mvcCurrentIResult.screenNode.actionUitrs', $scope.sync_screenNode_to_tempDataUitrs);
	//$scope.$watch('mvcCurrentIResult.screenNode.clickUitrs', $scope.sync_screenNode_to_tempDataUitrs);
	//$scope.$watch('mvcCurrentIResult.screenNode.screenElementChangeUitrs', $scope.sync_screenNode_to_tempDataUitrs);
	//$scope.$watch('mvcCurrentIResult.screenNode.userInputUitrs', $scope.sync_screenNode_to_tempDataUitrs);

	$scope.$watch('mvcCurrentIResult.screenNode', $scope.sync_screenNode_to_tempDataUitrs);

	$scope.updateAngucompleteRows = function (key) {
		angular.forEach($scope.mvcTempData.allUitrsRefsPagingView[key], function (uitr, index) {
			$scope.$broadcast('angucomplete-alt:changeInput', "pioPredictLabelResult-" + key + "-" + index, uitr.inputLabelName);
			if (uitr.userValues.length > 0)
				$scope.$broadcast('angucomplete-alt:changeInput', "userInuptValue-" + key + "-" + index, uitr.userValues[0].value);
			else
				$scope.$broadcast('angucomplete-alt:clearInput', "userInuptValue-" + key + "-" + index);
		})
	}

	$scope.updateScreenNameAutoComplete = function () {
		if (typeof $scope.mvcCurrentIResult.screenNode === "undefined") return; //in system init phase
		if ($scope.mvcCurrentIResult.screenNode.name !== "undefined" && $scope.mvcCurrentIResult.screenNode.name.length > 1)
			$scope.$broadcast('angucomplete-alt:changeInput', "ex14", $scope.mvcCurrentIResult.screenNode.name);
		else
			$scope.$broadcast('angucomplete-alt:clearInput', "ex14");
	}

	$scope.updateAllAngucompleteRows = function () {
		$scope.updateAngucompleteRows("SCREENJUMPER");
		$scope.updateAngucompleteRows("INSCREENJUMPER");
		$scope.updateAngucompleteRows("SCREENCHANGEUITR");
		$scope.updateAngucompleteRows("USERINPUT");
		$scope.updateAngucompleteRows("unPredicted");
		$scope.updateScreenNameAutoComplete();

	}
	$scope.updateObjectClassTag = function(fruit) {
		if (fruit.userInputType === "SCREENJUMPER") {
			fruit["@class"] = ".ScreenJumperElementTrainingRecord";

		} else if (fruit.userInputType === "INSCREENJUMPER") {
			fruit["@class"] = ".InScreenJumperTrainingRecord";

		} else if (fruit.userInputType === "SCREENCHANGEUITR") {
			fruit["@class"] = ".ScreenElementChangeUITR";

		} else {
			fruit["@class"] = ".UserInputTrainingRecord";

		}
	}
	$scope.userInputTypeChange = function (fruit, oldValue) {
		$scope.updateObjectClassTag(fruit);

		$scope.mvcTempData.allUitrsRefs[fruit.userInputType].push(fruit);
		var indexToDelete = $scope.mvcTempData.allUitrsRefs[oldValue].indexOf(fruit);
		//var lastIndex = indexToDelete==$scope.mvcTempData.allUitrsRefs[oldValue].length-1;
		$scope.mvcTempData.allUitrsRefs[oldValue].splice(indexToDelete, 1);
		//if (!lastIndex) {
		$scope.updateAngucompleteRows(oldValue);
		//};
		var oldNeo4jLabel = "";

		if (oldValue === "SCREENJUMPER") {
			oldNeo4jLabel = "ScreenJumperElementTrainingRecord";

		} else if (oldValue === "INSCREENJUMPER") {
			oldNeo4jLabel = "InScreenJumperTrainingRecord";

		} else if (oldValue === "SCREENCHANGEUITR") {
			oldNeo4jLabel = "ScreenElementChangeUITR";

		} else if (oldValue ==="USERINPUT") {
			oldNeo4jLabel = "UserInputTrainingRecord";
		}
		if (oldNeo4jLabel.length > 0 && fruit.graphId != null && fruit.graphId > 0)
			$scope.deleteNodeLabel(fruit.graphId, oldNeo4jLabel, fruit["@class"].substring(1));

	}
	$scope.nodeTriggeredByChange = function (fruit, oldValue) {
		$scope.updateObjectClassTag(fruit.triggeredBy);
		if (oldValue != "" && fruit.graphId) {
			$scope.deleteNodeTriggeredByRelationship(fruit.graphId, oldValue);
		}

	}

	$scope.screenTriggeredByUitrIdChange = function (screenId, oldUitrId) {

		if (oldUitrId != "" && typeof screenId != "undefined" && screenId != null && screenId != 0) {
			$scope.deleteUitrStepOutScreenRelationship(screenId, oldUitrId);
		}

	}

	$scope.testCaseNameSwitcher = function (newTestCaseName, oldTestCaseName) {
		angular.forEach($scope.mvcTempData.allUitrsRefs, function (value, key) {
			angular.forEach(value, function (uitr) {
				uitr.belongToCurrentTestCase = false;
				$scope.checkForDeletionCall(uitr, $scope.mvcCurrentIResult.domainName, "*", oldTestCaseName);
			})

		})

		for (var ind = 0; ind < $scope.mvcCurrentIResult.screenNode.testcases.length; ind++) {
			if ($scope.mvcCurrentIResult.screenNode.testcases[ind].endNode.graphId !== null && typeof $scope.mvcCurrentIResult.screenNode.testcases[ind].endNode.graphId !== "undefined" && $scope.mvcCurrentIResult.screenNode.testcases[ind].endNode.name === oldTestCaseName) {
				$scope.deleteInTestCaseRelationship($scope.mvcCurrentIResult.screenNode.graphId, $scope.mvcCurrentIResult.screenNode.testcases[ind].endNode.graphId);
				$scope.mvcCurrentIResult.screenNode.testcases = [];
				break;
			}
		}

	}

	$scope.checkForDeletionCall = function (fruit, domainName, testSuiteName, tcName) {
		if (fruit.graphId != 0 && typeof fruit.graphId != "undefined" && !fruit.belongToCurrentTestCase && fruit.testcases.length > 0) {

			var req = $scope.populateDeleteInTestCaseRequest(fruit,domainName, testSuiteName, tcName);

			$http(req).success(function (data, status, headers, config) {
				//TODO test cases need to be re-write to handle multiple test cases scenario
				if (data) {
					for (var ind = 0; ind < fruit.testcases.length; ind++) {
						if (fruit.testcases[ind].endNode.name === tcName) {
							fruit.testcases.splice(ind, 1);

						}
					}

					//TODO check if the $localStorage needs to be updated or not
					//if ($scope.mvcCurrentIResult.screenNode.graphId>0)
					//	$localStorage.mvcIResult[$localStorage.mvcIResult.indexOf($scope.mvcCurrentIResult)].
				}
				else
					alert("failed!");

				//return $q.resolve();
			}).error(function (data, status, headers, config) {
				alert("failure message: " + JSON.stringify({data: data}));
				//return $q.reject();
				//return false;
			});


		}
		return;
	}
	$scope.populateDeleteInTestCaseRequest = function (fruit, domainName, testSuiteName, tcName) {
		var tmpScreenName = $scope.parseScreenName();

		fruit.inputLabelName = fruit.pioPredictLabelResult.value;
		//if ($scope.fruits[ind].triggeredBy!=0 && typeof $scope.fruits[ind].triggeredBy!=="undefined") {
		//	$scope.fruits[ind].triggeredBy = $scope.getGraphNodeByGraphId($scope.fruits[ind].triggeredBy);
		//}
		if (fruit.userInputType === "SCREENJUMPER") {
			fruit["@class"] = ".ScreenJumperElementTrainingRecord";

		} else if (fruit.userInputType === "INSCREENJUMPER") {
			fruit["@class"] = ".InScreenJumperTrainingRecord";

		} else if (fruit.userInputType === "SCREENCHANGEUITR") {
			fruit["@class"] = ".ScreenElementChangeUITR";

		} else {
			fruit["@class"] = ".UserInputTrainingRecord";

		}


		var req = $scope.tebloudPostReq(
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.LIFE_PORTLET_API_PREFIX + '/' + domainName + "/" + testSuiteName + "/" + tcName + '/deleteInTestCaseRelationship',
			JSOG.encode(fruit)
		)
		return req;
	}


	$scope.parseScreenName = function () {
		var tmpScreenName = "";
		if (typeof $scope.mvcCurrentIResult.screenNode.name.originalObject !== 'undefined') {
			tmpScreenName = $scope.mvcCurrentIResult.screenNode.name.originalObject.name;
			$scope.mvcCurrentIResult.screenNode.name = tmpScreenName;
		}
		else tmpScreenName = $scope.mvcCurrentIResult.screenNode.name;
		return tmpScreenName;
	}

	$scope.populateSaveRequest = function (samePageUpdate) {
		$scope.parseScreenName();

		var allUitrsIn2DArr = $scope.getAllValues($scope.mvcTempData.allUitrsRefs);
		var tempArr = [].concat.apply([], allUitrsIn2DArr);

		for (var ind = 0; ind < tempArr.length; ind++) {
			tempArr[ind].inputLabelName = tempArr[ind].pioPredictLabelResult.value;
			if (tempArr[ind].userValues.length > 1) {
				tempArr[ind].userValues.length = 1;
			}
			if (tempArr[ind].userInputType === "SCREENJUMPER") {
				tempArr[ind]["@class"] = ".ScreenJumperElementTrainingRecord";

			} else if (tempArr[ind].userInputType === "INSCREENJUMPER") {
				tempArr[ind]["@class"] = ".InScreenJumperTrainingRecord";

			} else if (tempArr[ind].userInputType === "SCREENCHANGEUITR") {
				tempArr[ind]["@class"] = ".ScreenElementChangeUITR";

			} else {
				tempArr[ind]["@class"] = ".UserInputTrainingRecord";

			}
		}
		$scope.mvcCurrentIResult.screenNode.screenWidth = ate_global_page_context.screenWidth
		$scope.mvcCurrentIResult.screenNode.topDocumentWidth = ate_global_page_context.topDocumentWidth
		$scope.mvcCurrentIResult.screenNode.topDocumentHeight = ate_global_page_context.topDocumentHeight
		$scope.mvcCurrentIResult.samePageUpdate = samePageUpdate
		$scope.mvcCurrentIResult.screenNode.sourcingDoms = ate_global_page_context.pages
		if ($scope.mvcIResults.indexOf($scope.mvcCurrentIResult) > 0) {
			$scope.mvcCurrentIResult.lastScreenNodeIntermediateResult = $scope.mvcIResults[$scope.mvcIResults.indexOf($scope.mvcCurrentIResult) - 1]

		}
		var req = $scope.tebloudPostReq(
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() +'/saveIntermediateResult',
			$scope.mvcCurrentIResult
		);

		req.data = JSOG.encode(req.data);
		return req;
	}

	$scope.compressHtmlSources = function (sourceDoms) {

		for (var index = 0; index < sourceDoms.length; index++) {
			sourceDoms[index].domDoc = LZString.compress(sourceDoms[index].domDoc);
			sourceDoms[index].domText = LZString.compress(sourceDoms[index].domText);
		}
		return sourceDoms;
	}

	$scope.uncompressHtmlSources = function (sourceDoms) {

		for (var index = 0; index < sourceDoms.length; index++) {
			sourceDoms[index].domDoc = LZString.decompress(sourceDoms[index].domDoc);
			sourceDoms[index].domText = LZString.decompress(sourceDoms[index].domText);
		}
		return sourceDoms;
	}

	$scope.saveIntermediateResult = function (samePageUpdate) {

		if (!$scope.verifyRegularScreenInputs(samePageUpdate)) return $q.reject();


		var req = $scope.populateSaveRequest(samePageUpdate);

		$http(req).success(function (data, status, headers, config) {
			data = JSOG.decode(data);
			//workaround about the bad html parser in java lib, which will convert element node to a text node.
			//we will store the never converted html sources for next screen.
			data.screenNode.sourcingDoms = $scope.mvcCurrentIResult.screenNode.sourcingDoms;

			$scope.mvcIResults[$scope.mvcIResults.indexOf($scope.mvcCurrentIResult)] = data;
			$scope.mvcCurrentIResult = data;
			//initMvcTempData();
			$scope.mvcUIOperationStatus.pageNotSavedYet = false;
			//if (samePageUpdate)
			//	$localStorage.mvcIResults[$localStorage.mvcIResults.indexOf($scope.mvcCurrentIResult)] = data;
			//else {
			//	$localStorage.mvcIResults.push($scope.mvcCurrentIResult);
			//}

			if ($scope.mvcIResults.indexOf($scope.mvcCurrentIResult) > 0) {
				$scope.mvcCurrentIResult.lastScreenNodeIntermediateResult.screenNode.sourcingDoms = $scope.mvcIResults[$scope.mvcIResults.indexOf($scope.mvcCurrentIResult) - 1].screenNode.sourcingDoms;
				//$localStorage.mvcIResults[$scope.mvcIResults.indexOf($scope.mvcCurrentIResult) - 1] = data;
				$scope.mvcIResults[$scope.mvcIResults.indexOf($scope.mvcCurrentIResult) - 1] = $scope.mvcCurrentIResult.lastScreenNodeIntermediateResult;

			}
			$scope.$storage.mvcIResults = LZString.compress(JSON.stringify(JSOG.encode($scope.mvcIResults)));
			//ate_global_page_context.pages = data.domStrings;

			//if (typeof $localStorage.currentScreenNode == "undefined"
			//	|| $localStorage.currentScreenNode.data.screenNodeNeo4jId == $localStorage.lastScreenNode.screenNodeNeo4jId)
			//	$scope.previousScreenOverridedByCurrentScreen = true;

			alert("success!");

			//return $q.resolve();
		}).error(function (data, status, headers, config) {
			//if (typeof $localStorage.lastScreenNodeBk !="undefined" && typeof $localStorage.lastScreenNodeBk.screenNodeNeo4jId !="undefined")
			//{
			//	$localStorage.lastScreenNode = $localStorage.lastScreenNodeBk;
			//}
			alert("failure message: " + JSON.stringify({data: data}));
			//return $q.reject();
			//return false;
		});


	}

	$scope.updateScreenContext = function() {
		$scope.resetUiOperationStatus($scope.mvcCurrentIResult);
		$scope.sync_screenNode_to_tempDataUitrs();
		$scope.reset_ate_global_context_data($scope.mvcCurrentIResult);
	}
	$scope.loadPreviousScreen = function () {

		if ($scope.mvcIResults.indexOf($scope.mvcCurrentIResult) > 0) {
			var mycontinue = true;
			if ($scope.mvcUIOperationStatus.pageNotSavedYet == true) {
				mycontinue = confirm("screen not saved yet, continue?");
			}
			if (mycontinue) {
				$scope.mvcCurrentIResult = $scope.mvcIResults[$scope.mvcIResults.indexOf($scope.mvcCurrentIResult) - 1];
				$scope.updateScreenContext();
			}

		} else {
			alert("there is no previous screen")
		}


	}


	$scope.resetUiOperationStatus = function (mvcCurrentIResult) {
		if (mvcCurrentIResult.screenNode.graphId === null || typeof mvcCurrentIResult.screenNode.graphId === "undefined") {
			$scope.mvcUIOperationStatus.pageNotSavedYet = true;
			$scope.mvcUIOperationStatus.newScreenModelCreated = true;
		} else {
			$scope.mvcUIOperationStatus.pageNotSavedYet = false;
			$scope.mvcUIOperationStatus.newScreenModelCreated = false;
		}
		$scope.mvcUIOperationStatus.spinneractive = false;
		$scope.mvcUIOperationStatus.lastScreenOfTestCase = false;
	}

	$scope.reset_ate_global_context_data = function (mvcCurrentIResult) {
		if (mvcCurrentIResult.screenNode.graphId === null || typeof mvcCurrentIResult.screenNode.graphId === "undefined") {
			alert("system will re-inject the js to page for ate global page context recover.");
			$scope.injectProcessor();
		} else {
			if (typeof window.ate_global_page_context === "undefined") {
				window.ate_global_page_context = {};
			}

			ate_global_page_context.pages = mvcCurrentIResult.screenNode.sourcingDoms
			ate_global_page_context.screenWidth = mvcCurrentIResult.screenNode.screenWidth;
			ate_global_page_context.topDocumentWidth = mvcCurrentIResult.screenNode.topDocumentWidth;
			ate_global_page_context.topDocumentHeight = mvcCurrentIResult.screenNode.topDocumentHeight;
			ate_global_page_context.screenUrl = mvcCurrentIResult.screenNode.url;

			ate_global_page_context.domain = mvcCurrentIResult.domainName;
			ate_global_page_context.domainProtocol = mvcCurrentIResult.domainProtocol;
			ate_global_page_context.domainPort = mvcCurrentIResult.domainPort;


		}
	}
	$scope.loadCurrentScreen = function () {
		if ($scope.mvcIResults.indexOf($scope.mvcCurrentIResult) < $scope.mvcIResults.length - 1) {
			var mycontinue = true;
			if ($scope.mvcUIOperationStatus.pageNotSavedYet == true) {
				mycontinue = confirm("screen not saved yet, continue?");
			}
			if (mycontinue) {
				$scope.mvcCurrentIResult = $scope.mvcIResults[$scope.mvcIResults.indexOf($scope.mvcCurrentIResult) + 1];
				$scope.resetUiOperationStatus($scope.mvcCurrentIResult);
				$scope.sync_screenNode_to_tempDataUitrs();
				$scope.reset_ate_global_context_data($scope.mvcCurrentIResult);

			}

		} else {
			alert("there is no next screen")
		}

	}

	$scope.verifyPioPredictScreenInputs = function () {

		if ($scope.mvcCurrentIResult.testCaseName === null || typeof $scope.mvcCurrentIResult.testCaseName === "undefined") {
			alert("testcase name is empty");
			return false;
		}

		return true;

	}
	$scope.tebloudPutReq = function(tebloudApiUrl, postData) {
		return {
			method: 'PUT',
			url: tebloudApiUrl,
			headers: {'Content-Type': 'application/json'},
			//data: ate_global_page_documents
			data: postData,
			params: {'s': localStorage.getItem('session_id')}
		}
	}
	$scope.deleteNodeLabel = function (graphId, labelNameToDelete, newLabelToAdd) {

		var req = $scope.tebloudPutReq (
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT  + $scope.TCG_URL_PART1() + '/node/' + graphId + '/label/' + labelNameToDelete,
			{newLabel: newLabelToAdd}
		);
		$http(req).success(function (data, status, headers, config) {
			if (data == 1 || data == 2) {
				alert("node label in db is updated");
				var relName = "PREDICTED_USER_INPUT_RESULTS";
				switch (labelNameToDelete) {
					case "ScreenJumperElementTrainingRecord":
						relName = "PREDICTED_USER_SCREENJUMP_ELEMENT_RESULTS";
						break;
					case "InScreenJumperTrainingRecord":
						relName = "PREDICTED_USER_IN_SCREENJUMP_ELEMENT_RESULTS";
						break;
					case "ScreenElementChangeUITR":
						relName = "PREDICTED_USER_SCREEN_ELEMENT_CHANGE_RESULTS";
						break;
					default:
						relName = "PREDICTED_USER_INPUT_RESULTS";
						break;
				}
				$scope.deleteNodeRelationship($scope.mvcCurrentIResult.screenNode.graphId, graphId, relName);
			}

		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});

	}

	$scope.tebloudDeleteReq = function(tebloudApiUrl, postData) {
		return {
			method: 'DELETE',
			url: tebloudApiUrl,
			headers: {'Content-Type': 'application/json'},
			//data: ate_global_page_documents
			data: postData,
			params: {'s': localStorage.getItem('session_id')}
		}
	}
	$scope.deleteUitrUserValue = function (userValueGraphId, uitGraphId) {

		var req = $scope.tebloudDeleteReq(
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.LIFE_PORTLET_API_PREFIX + '/node/' + userValueGraphId + '/USER_VALUES/',
			{relatedNodeGraphId: uitGraphId}
		)
		$http(req).success(function (data, status, headers, config) {
			if (data == 1 || data == 2) {
				alert("node in db is deleted");

			} else {
				alert("db might has integrity issue in user value node: " + userValueGraphId + " related to node: " + uitGraphId);
			}

		}).error(function (data, status, headers, config) {

			alert("failure message: " + JSON.stringify({data: data}));

		});

	}

	$scope.deleteInTestCaseRelationship = function (nodeId, testCaseNodeId) {

		var req = $scope.tebloudDeleteReq (
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.LIFE_PORTLET_API_PREFIX + '/rel/' + nodeId + '/IN_TESTCASE/' + testCaseNodeId,
			{'Content-Type': 'application/json'}
		)
		$http(req).success(function (data, status, headers, config) {
			if (data == 1 || data == 2)
				alert("old in test case relationship in db is deleted");

		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});

	}

	$scope.deleteUitrStepOutScreenRelationship = function (screenId, triggerUitrId) {

		var req = $scope.tebloudDeleteReq(
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.LIFE_PORTLET_API_PREFIX + '/rel/' + triggerUitrId + '/STEP_OUT/' + screenId,
			{'Content-Type': 'application/json'}
		)
		$http(req).success(function (data, status, headers, config) {
			if (data == 1 || data == 2)
				alert("old screen stepped out relationship in db is deleted");

		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});

	}

	$scope.deleteNodeTriggeredByRelationship = function (graphId, triggerGraphId) {

		var req = $scope.tebloudDeleteReq (
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() + '/rel/' + graphId + '/TRIGGERED_BY/' + triggerGraphId,
			{'Content-Type': 'application/json'}
		)
		$http(req).success(function (data, status, headers, config) {
			if (data == 1 || data == 2)
				alert("old node triggeredby relationship in db is deleted");

		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});

	}

	$scope.deleteNodeRelationship = function (startNodeId, endNodeId, relationshipName) {

		var req = $scope.tebloudDeleteReq (
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() + '/rel/' + startNodeId + '/' + relationshipName + '/' + endNodeId,
			{'Content-Type': 'application/json'}
		)
		$http(req).success(function (data, status, headers, config) {
			if (data == 1 || data == 2)
				alert("old node relationship in db is deleted");

		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});

	}

	$scope.JSOGEncodeUitrs = function () {
		angular.forEach($scope.mvcTempData.allUitrsRefs, function (uitrs, key) {
			angular.forEach(uitrs, function (uitr, index) {
				$scope.mvcTempData.allUitrsRefs[key][index] = JSOG.encode(uitr);
			});
		})
	}

	$scope.pioPredict = function () {
		if (!$scope.verifyPioPredictScreenInputs()) return $q.reject();
		$scope.JSOGEncodeUitrs();
		/*angular.forEach($scope.mvcTempData.allUitrsRefs, function(uitrs, key){
		 angular.forEach(uitrs, function(uitr,index) {
		 $scope.mvcTempData.allUitrsRefs[key][index] = JSOG.encode(uitr);
		 });
		 })*/

		var req = $scope.tebloudPostReq (
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() + '/uitrs/pioPredict',

			$scope.mvcTempData.allUitrsRefs
		)
		$http(req).success(function (data, status, headers, config) {
			data = JSOG.decode(data)
			angular.forEach(data, function (uitrs, key) {
				angular.forEach(uitrs, function (uitr) {
					uitr.inputMLHtmlCodeChangedByUI = false;
					if (uitr.userValues.length > 0)
						$scope.sync_page_element(uitr.userValues[0].value, $scope.parseInputGuid(uitr.elementCoreHtmlCode));
				})
			})
			//never replace the reference pointer for the allUitrsRefs, it might cause problem for veiew update
			$scope.fillOutTempDataUitrsRefsArrays(data);
			$scope.syncMvcTempDataToScreenNodeModel();
		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});

	};
	$scope.removeAllUitrs = function () {
		$scope.mvcTempData.allUitrsRefs["unPredicted"] = [];
		$scope.mvcTempData.allUitrsRefs["SCREENCHANGEUITR"] = [];
		$scope.mvcTempData.allUitrsRefs["SCREENJUMPER"] = [];
		$scope.mvcTempData.allUitrsRefs["USERINPUT"] = [];
		$scope.mvcTempData.allUitrsRefs["INSCREENJUMPER"] = [];
	}
	$scope.fillOutTempDataUitrsRefsArrays = function (data) {
		$scope.mvcTempData.allUitrsRefs["unPredicted"] = data["unPredicted"];
		$scope.mvcTempData.allUitrsRefs["SCREENCHANGEUITR"] = data["SCREENCHANGEUITR"];
		$scope.mvcTempData.allUitrsRefs["SCREENJUMPER"] = data["SCREENJUMPER"];
		$scope.mvcTempData.allUitrsRefs["USERINPUT"] = data["USERINPUT"];
		$scope.mvcTempData.allUitrsRefs["INSCREENJUMPER"] = data["INSCREENJUMPER"];
	}
	$scope.pagePredict = function () {
		var req = {};
		var currentIResultIndex = $scope.mvcIResults.indexOf($scope.mvcCurrentIResult);
		if (currentIResultIndex > 0) {
			req = $scope.tebloudPostReq(
				'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() +'/pagePredict',
				 {
					newDoms: ate_global_page_context.pages,
					previousDoms: $scope.mvcIResults[currentIResultIndex - 1].screenNode.sourcingDoms
				}
			)
		} else {
			req = $scope.tebloudPostReq(
				'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() +'/pagePredict',
				{newDoms: ate_global_page_context.pages, previousDoms: []}
			)
		}
		$http(req).success(function (data, status, headers, config) {
			$scope.mvcTempData.predictedScreenNames = data;
			$scope.mvcCurrentIResult.screenNode.screenPredictStrategy = Object.keys($scope.mvcTempData.predictedScreenNames)[0];
			$scope.mvcCurrentIResult.screenNode.name = Object.keys($scope.mvcTempData.predictedScreenNames[Object.keys($scope.mvcTempData.predictedScreenNames)[0]])[0];
			$scope.mvcCurrentIResult.screenNode.inputMLHtmlWords = $scope.mvcTempData.predictedScreenNames[$scope.mvcCurrentIResult.screenNode.screenPredictStrategy][$scope.mvcCurrentIResult.screenNode.name].screenTrainingWords;

		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});

	};

	$scope.getObjectValues = function (obj) {
		var keys = Object.keys(obj);
		var values = [];
		for (var key in keys) {
			values.push(obj[key]);
		}
	}


	$scope.tebloudGetReq = function(tebloudApiUrl) {
		return {
			method: 'GET',
			url: tebloudApiUrl,
			headers: {'Content-Type': 'application/json'},
			params: {'s': localStorage.getItem('session_id')}
		}
	}
	$scope.queryAllUserInputValues = function () {
		var req = $scope.tebloudGetReq(
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() + '/queryUserInputValues'
		)
		$http(req).success(function (data, status, headers, config) {
			$scope.mvcTempData.allUserValues = $scope.uniquearr(data.userValues);
		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});

	}

	$scope.queryPioPredictedFieldNames = function () {
		var req = $scope.tebloudGetReq(
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() + '/queryPioPredictedFieldNames'
//				 data: {content: document.documentElement.innerHTML}
			)
		$http(req).success(function (data, status, headers, config) {
			$scope.mvcTempData.allFieldNames = $scope.uniquearr(data.fieldNames);
		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});

	}
	$scope.uniquearr = function (origArr) {
		var newArr = [];
		var origLen = origArr.length;
		var found = false;
		var x, y;

		for (x = 0; x < origLen; x++) {
			//found = "undefined";
			for (y = 0; y < newArr.length; y++) {
				if (JSON.stringify(origArr[x]) === JSON.stringify(newArr[y])) {
					found = true;
					break;
				}
			}
			if (!found) {
				newArr.push(origArr[x]);
			}
			found = false;
		}
		return newArr;
	}
	$scope.queryAllScreenNames = function () {
		var req = $scope.tebloudGetReq(
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() + '/queryScreenNames')
		$http(req).success(function (data, status, headers, config) {
			$scope.mvcTempData.screenNames = $scope.uniquearr(data.screenNames);
		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});

	}
	$scope.queryAllTestSuiteNames = function () {
		var req = $scope.tebloudGetReq(
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() + '/queryTestSuiteNames')
		$http(req).success(function (data, status, headers, config) {
			$scope.mvcTempData.allTestSuiteNames = $scope.uniquearr(data.suiteNames);
		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});

	}

	$scope.queryAllTestCaseNames = function() {

		var req = $scope.tebloudGetReq(
			'http://' + $scope.TCG_SERVER_IP + ':' + $scope.TCG_SERVER_PORT + $scope.TCG_URL_PART1() + '/queryTestCaseNames')
		$http(req).success(function (data, status, headers, config) {
			$scope.mvcTempData.allTestCaseNames = $scope.uniquearr(data.testCaseNames);
		}).error(function (data, status, headers, config) {
			alert("failure message: " + JSON.stringify({data: data}));
		});
	}

	//autocomplete

	$scope.remoteUrlRequestFn = function (str) {
		return {q: str};
	};


	$scope.queryAllScreenNames();
	$scope.queryAllTestSuiteNames();
	$scope.queryAllTestCaseNames();
	$scope.queryAllUserInputValues();
	$scope.queryPioPredictedFieldNames();

	$scope.popupStyle = [];

	$scope.showPopover = function(key, index) {
		$scope.popupStyle[key][index] = {"position": "absolute", "z-index": "10000","background-color": "red", opacity:1 };
	};

	$scope.hidePopover = function (key, index) {
		$scope.popupStyle[key][index] = {width:"250px", height:"40px", overflow:"auto"};
	};


// and fire it after definition
/**********************************************MVC****************************************/
	$scope.newScreenModel =function(iResultTemplate) {
		if ($scope.mvcIResults.length>0 && (typeof $scope.mvcIResults[$scope.mvcIResults.length - 1].screenNode.graphId === "undefine" ||
			$scope.mvcIResults[$scope.mvcIResults.length - 1].screenNode.graphId === null)) {
			$scope.mvcIResults[$scope.mvcIResults.length - 1] = iResultTemplate;
		} else {
			$scope.mvcIResults.push(iResultTemplate);
		}
		$scope.mvcCurrentIResult = $scope.mvcIResults[$scope.mvcIResults.length-1];
		var indexOfCurrentIResult = $scope.mvcIResults.indexOf($scope.mvcCurrentIResult);
		if (indexOfCurrentIResult>0) {
			$scope.mvcCurrentIResult.testCaseName = $scope.mvcIResults[indexOfCurrentIResult - 1].testCaseName;
			angular.copy($scope.mvcIResults[indexOfCurrentIResult - 1].testSuitesMap, $scope.mvcCurrentIResult.testSuitesMap);
		}
		$scope.initMvcTempData();
	 	$scope.mvcUIOperationStatus.newScreenModelCreated = true;
	 	//$scope.update();
	}
	$scope.buildScreenModel =function(iResult) {
		var noFound = true;
		for (var ind = 0; ind<$scope.mvcIResults.length; ind++) {
			if ($scope.mvcIResults[ind].screenNode.graphId && $scope.mvcIResults[ind].screenNode.graphId == iResult.screenNode.graphId) {
				noFound = false;
				$scope.mvcIResults[ind] = iResult;

				break;
			}
		}
		if (noFound) {
			if ($scope.mvcIResults.length>0 && !$scope.mvcIResults[$scope.mvcIResults.length - 1].screenNode.graphId){
				$scope.mvcIResults[$scope.mvcIResults.length - 1] = iResult;
			} else {
				$scope.mvcIResults.push(iResult);
			}
		}
		$scope.mvcCurrentIResult = iResult;
		$scope.updateScreenContext();
		//$scope.initMvcTempData();

	}
	$scope.mvcGetIResultTemplate = function() {
		var req = $scope.tebloudGetReq(
			'http://' + $scope.TCG_SERVER_IP +':' +$scope.TCG_SERVER_PORT + $scope.LIFE_PORTLET_API_PREFIX + '/screenNode/getIResultTemplate')
		$http(req).success(function(data, status, headers, config) {
			data = JSOG.decode(data);
			$scope.newScreenModel(data);

		}).error(function(data, status, headers, config) {
			alert( "failure message when get IResult Template: " + JSON.stringify({data: data}));
		});

	}

	$scope.mvcInitSystemProperties = function() {
		var req = $scope.tebloudGetReq(
			'http://' + $scope.TCG_SERVER_IP +':' +$scope.TCG_SERVER_PORT + $scope.LIFE_PORTLET_API_PREFIX + '/system/properties/runningMode')
		$http(req).success(function(data, status, headers, config) {
			data = JSOG.decode(data);
			$scope.mvcUIOperationStatus.runningModeProperties = data;

		}).error(function(data, status, headers, config) {
			alert( "failure message when init system properties: " + JSON.stringify({data: data}));
		});

	}

	$scope.mvcGetIResult = function(screenNodeId, testcasename) {
		if (!testcasename) {
			alert("test case name is not set");
			return false;
		}
		if (!$scope.mvcCurrentIResult.domainName) {
			alert("domain name is not set");
			return false;
		}
		var req = $scope.tebloudGetReq('http://' + $scope.TCG_SERVER_IP +':' +$scope.TCG_SERVER_PORT + $scope.LIFE_PORTLET_API_PREFIX + '/screenNode/'+screenNodeId+"/"+testcasename);
		$http(req).success(function(data, status, headers, config) {
			data = JSOG.decode(data);
			$scope.buildScreenModel(data);

		}).error(function(data, status, headers, config) {
			alert( "failure message when get IResult: " + JSON.stringify({data: data}));
		});

	}

	$scope.clickPreviousUitrPage = function() {

		if ($scope.mvcTempData.uitrPagingVars.currentPage > 0) {
			$scope.mvcTempData.uitrPagingVars.currentPage = $scope.mvcTempData.uitrPagingVars.currentPage - 1;

			$scope.resetUitrsPagingVars($scope.mvcTempData.uitrPagingVars.currentPage);

			$scope.updateUitrsPagingView();
		}
	}
	$scope.clickNextUitrPage = function() {
		if ($scope.mvcTempData.uitrPagingVars.currentPage < $scope.mvcTempData.uitrPagingVars.numberOfPages - 1) {
			$scope.mvcTempData.uitrPagingVars.currentPage = $scope.mvcTempData.uitrPagingVars.currentPage + 1;

			$scope.resetUitrsPagingVars($scope.mvcTempData.uitrPagingVars.currentPage);
			$scope.updateUitrsPagingView();
		}
	}
	$scope.calculateAbsoluteIndex = function(userInputTypeKey, relativeIndex) {
		var uitr  = $scope.mvcTempData.allUitrsRefsPagingView[userInputTypeKey][relativeIndex];
		return $scope.mvcTempData.allUitrsRefs[userInputTypeKey].indexOf(uitr);
	}
	$scope.resetUitrsPagingVars = function(currentPage) {
		if (!$scope.mvcTempData.uitrPagingVars)
				$scope.mvcTempData.uitrPagingVars = {};

		$scope.mvcTempData.uitrPagingVars.userInputTypeKeys = Object.keys($scope.mvcTempData.allUitrsRefs);
		$scope.mvcTempData.uitrPagingVars.totalUitrsCount = [].concat.apply([], $scope.getAllValues($scope.mvcTempData.allUitrsRefs)).length;
		$scope.mvcTempData.uitrPagingVars.pageSize = 10;
		$scope.mvcTempData.uitrPagingVars.numberOfPages =
			Math.ceil($scope.mvcTempData.uitrPagingVars.totalUitrsCount / $scope.mvcTempData.uitrPagingVars.pageSize);

		$scope.mvcTempData.uitrPagingVars.currentPage = currentPage;

		if ($scope.mvcTempData.uitrPagingVars.numberOfPages>=currentPage+1) {

			$scope.mvcTempData.uitrPagingVars.currentUitrUserInputTypeKeyStartIndex = 0;

			$scope.mvcTempData.uitrPagingVars.currentUitrUserInputValueArrayStartIndex = 0;
			//var currentUitrUserInputTypeStartKey = $scope.mvcTempData.uitrPagingVars.userInputTypeKeys[$scope.mvcTempData.uitrPagingVars.currentUitrUserInputTypeKeyStartIndex];
			var uitrCount = 0;
			var uitrUserInputTypeKey;
			var index;
			for (index = 0; index<$scope.mvcTempData.uitrPagingVars.userInputTypeKeys.length; index++) {
				uitrUserInputTypeKey =$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[index];
				uitrCount = uitrCount + $scope.mvcTempData.allUitrsRefs[uitrUserInputTypeKey].length;
				if (uitrCount>$scope.mvcTempData.uitrPagingVars.pageSize * $scope.mvcTempData.uitrPagingVars.currentPage ) {
					$scope.mvcTempData.uitrPagingVars.currentUitrUserInputTypeKeyStartIndex = index;
					$scope.mvcTempData.uitrPagingVars.currentUitrUserInputValueArrayStartIndex =
						$scope.mvcTempData.allUitrsRefs[uitrUserInputTypeKey].length - (uitrCount-$scope.mvcTempData.uitrPagingVars.pageSize*$scope.mvcTempData.uitrPagingVars.currentPage);
					break;

				}
			}
			uitrCount = 0;
			var totalCount = 0;
			for (index = 0; index<$scope.mvcTempData.uitrPagingVars.userInputTypeKeys.length; index++) {
				uitrUserInputTypeKey =$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[index];
				totalCount = totalCount + $scope.mvcTempData.allUitrsRefs[uitrUserInputTypeKey].length;
			}
			for (index = 0; index<$scope.mvcTempData.uitrPagingVars.userInputTypeKeys.length; index++) {
				uitrUserInputTypeKey =$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[index];
				uitrCount = uitrCount + $scope.mvcTempData.allUitrsRefs[uitrUserInputTypeKey].length;
				if (uitrCount>=$scope.mvcTempData.uitrPagingVars.pageSize * ($scope.mvcTempData.uitrPagingVars.currentPage + 1)) {
					$scope.mvcTempData.uitrPagingVars.currentUitrUserInputValueArrayEndIndex =
						$scope.mvcTempData.allUitrsRefs[uitrUserInputTypeKey].length - (uitrCount- $scope.mvcTempData.uitrPagingVars.pageSize*($scope.mvcTempData.uitrPagingVars.currentPage+1)) - 1;
					break;
				}
				if (index == $scope.mvcTempData.uitrPagingVars.userInputTypeKeys.length-1 || totalCount == uitrCount) {
					$scope.mvcTempData.uitrPagingVars.currentUitrUserInputValueArrayEndIndex =
						$scope.mvcTempData.allUitrsRefs[uitrUserInputTypeKey].length - 1;
					break;
				}
			}
			$scope.mvcTempData.uitrPagingVars.currentUitrUserInputTypeKeyEndIndex = index;
		} else {
			$scope.mvcTempData.uitrPagingVars.currentUitrUserInputTypeKeyStartIndex = 0;
			$scope.mvcTempData.uitrPagingVars.currentUitrUserInputTypeKeyEndIndex = 0;
			$scope.mvcTempData.uitrPagingVars.currentUitrUserInputValueArrayStartIndex = 0;
			$scope.mvcTempData.uitrPagingVars.currentUitrUserInputValueArrayEndIndex = 0;
		}

	}
	$scope.updateUitrsPagingView = function(){
		$scope.mvcTempData.allUitrsRefsPagingView = {};
		if ($scope.mvcTempData.uitrPagingVars.numberOfPages>1) {
			for (var index=$scope.mvcTempData.uitrPagingVars.currentUitrUserInputTypeKeyStartIndex;
				 index<=$scope.mvcTempData.uitrPagingVars.currentUitrUserInputTypeKeyEndIndex; index++) {
				if (index ==$scope.mvcTempData.uitrPagingVars.currentUitrUserInputTypeKeyStartIndex ) {
					if (index == $scope.mvcTempData.uitrPagingVars.currentUitrUserInputTypeKeyEndIndex) {
						$scope.mvcTempData.allUitrsRefsPagingView[$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[index]] =
							$scope.mvcTempData.allUitrsRefs[$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[index]]
								.slice($scope.mvcTempData.uitrPagingVars.currentUitrUserInputValueArrayStartIndex,
								$scope.mvcTempData.uitrPagingVars.currentUitrUserInputValueArrayEndIndex + 1);
					} else {
						$scope.mvcTempData.allUitrsRefsPagingView[$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[index]] =
							$scope.mvcTempData.allUitrsRefs[$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[index]]
								.slice($scope.mvcTempData.uitrPagingVars.currentUitrUserInputValueArrayStartIndex,
								$scope.mvcTempData.allUitrsRefs[$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[index]].length);
					}

				} else if (index==$scope.mvcTempData.uitrPagingVars.currentUitrUserInputTypeKeyEndIndex) {
					$scope.mvcTempData.allUitrsRefsPagingView[$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[index]] =
						$scope.mvcTempData.allUitrsRefs[$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[index]]
							.slice(0, $scope.mvcTempData.uitrPagingVars.currentUitrUserInputValueArrayEndIndex + 1);
				} else {
					$scope.mvcTempData.allUitrsRefsPagingView[$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[index]] =
						$scope.mvcTempData.allUitrsRefs[$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[index]];
				}
			}
		} else {

			for (var key in $scope.mvcTempData.uitrPagingVars.userInputTypeKeys)
				$scope.mvcTempData.allUitrsRefsPagingView[$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[key]] = $scope.mvcTempData.allUitrsRefs[$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[key]];
		}
		$scope.updateAllAngucompleteRows();

	}
	$scope.initMvcTempData = function() {
		$scope.mvcTempData.allUitrsRefs["unPredicted"]=[];
		if (typeof $scope.mvcCurrentIResult.screenNode!=="undefined") {
			$scope.mvcTempData.allUitrsRefs["SCREENCHANGEUITR"] = $scope.mvcCurrentIResult.screenNode.screenElementChangeUitrs;
			$scope.mvcTempData.allUitrsRefs["SCREENJUMPER"] = $scope.mvcCurrentIResult.screenNode.actionUitrs;
			$scope.mvcTempData.allUitrsRefs["USERINPUT"] = $scope.mvcCurrentIResult.screenNode.userInputUitrs;
			$scope.mvcTempData.allUitrsRefs["INSCREENJUMPER"] = $scope.mvcCurrentIResult.screenNode.clickUitrs;
		} else {
			$scope.mvcTempData.allUitrsRefs["SCREENCHANGEUITR"] = [];
			$scope.mvcTempData.allUitrsRefs["SCREENJUMPER"] = [];
			$scope.mvcTempData.allUitrsRefs["USERINPUT"] = [];
			$scope.mvcTempData.allUitrsRefs["INSCREENJUMPER"] = [];
		}
		//$scope.resetUitrsPagingVars(0);
		//$scope.updateUitrsPagingView();
		$scope.initPopStyleArray();
		//$scope.mvcTempData.purposeOfInject = "undefined";
		//$scope.mvcTempData.injectTimer = "undefined";
	}
	$scope.syncMvcTempDataToScreenNodeModel = function() {
		$scope.mvcCurrentIResult.screenNode.screenElementChangeUitrs = $scope.mvcTempData.allUitrsRefs["SCREENCHANGEUITR"]
		$scope.mvcCurrentIResult.screenNode.actionUitrs = $scope.mvcTempData.allUitrsRefs["SCREENJUMPER"];
		$scope.mvcCurrentIResult.screenNode.userInputUitrs = $scope.mvcTempData.allUitrsRefs["USERINPUT"];
		$scope.mvcCurrentIResult.screenNode.clickUitrs = $scope.mvcTempData.allUitrsRefs["INSCREENJUMPER"];
	}
	$scope.mvcInit = function() {
		if (typeof $scope.$storage.mvcIResults!=="undefined") {
			//angular.forEach($scope.$storage.mvcIResults, function(mvcIR){
			//	//void max call statck reached error
			//	$scope.mvcIResults.push(JSOG.decode(mvcIR));
			//})
			$scope.mvcIResults = JSOG.decode(JSON.parse(LZString.decompress($scope.$storage.mvcIResults)));
			//$scope.mvcCurrentIResult = $scope.mvcIResults[$scope.mvcIResults.length-1];

		} else {
			$scope.$storage.mvcIResults = LZString.compress(JSON.stringify(JSOG.encode([])));
		}
		$scope.mvcGetIResultTemplate();
		$scope.mvcInitSystemProperties();
	}
	$scope.mvcInit();
	$scope.initPopStyleArray = function() {
		for (var indj = 0; indj < $scope.mvcTempData.uitrPagingVars.userInputTypeKeys.length; indj++) {
			$scope.popupStyle[$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[indj]]=[]
			for (var ind = 0; ind < 150; ind++) {

				$scope.popupStyle[$scope.mvcTempData.uitrPagingVars.userInputTypeKeys[indj]].push({
					width: "250px",
					height: "40px",
					overflow: "auto"
				});
			}
		}
	}


};
app1.controller('ModalInstanceCtrl', function ($uibModalInstance, screenTrainingWords) {
 var $ctrl = this;
 $ctrl.screenTrainingWords = screenTrainingWords;

 $ctrl.ok = function () {
 $uibModalInstance.close();
 };


 });
