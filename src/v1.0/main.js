//(function () {
//    'use strict';


var APP_SERVER_URL = 'http://peidong-3442:9080';
var LIFE_PORTLET_API_PREFIX = '/tebloud-tcg-liferay-portlet/services';

angular
    .module('app')
    .controller('ScreenNamesController', ScreenNamesController);

    ScreenNamesController.$inject = ['$scope', '$rootScope', '$http', '$location'];
    function ScreenNamesController($scope, $rootScope, $http, $location) {
        var url = APP_SERVER_URL + LIFE_PORTLET_API_PREFIX + '/queryScreenNames';
        $http({
            method: 'GET',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            url: url
        }).then(function (response) {
            $scope.screenNames = response.data;
        }, function (response) {
        });


    }
    
    
    angular
        .module('app')
        .controller('FoodListController', FoodListController);

    FoodListController.$inject = ['$scope', '$rootScope', '$http', '$location'];
    function FoodListController($scope, $rootScope, $http, $location) {
        var url = APP_SERVER_URL + '/app-portal/menu';
        $http({
            method: 'GET',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            url: url
        }).then(function (response) {
            $scope.food = response.data;
        }, function (response) {
        });

        $scope.getTotal = function () {
            $scope.cartTotal = 0;
            for (var i = 0; i < $scope.cartItems.length; ++i) {
                $scope.cartTotal += parseInt($scope.cartItems[i].menuItem.price)*$scope.cartItems[i].quantity;
            }
        };

        $scope.cartTotal = 0;

        if (localStorage.getItem('cartItems')) {
            $scope.cartItems = JSON.parse(localStorage.getItem('cartItems'));
            $scope.getTotal();
        } else {
            $scope.cartItems = [];
            localStorage.setItem('cartItems', '');
        }

        checkCart();

        $scope.addToCart = function (id) {
            var isNew = true;
            for (var i = 0; i < $scope.food.length; ++i) {
                if ($scope.food[i].id == id) {
                    for (var j = 0; j < $scope.cartItems.length; ++j) {
                        if ($scope.cartItems[j].menuItem.id == id) {
                            $scope.cartItems[j].quantity = $scope.cartItems[j].quantity + 1;
                            isNew = false;
                        }
                    }
                    if (isNew) {
                        $scope.cartItems.push({
                            'id': id,
                            'quantity': 1,
                            'menuItem': $scope.food[i]
                        });
                    }
                }
            }
            checkCart();
            $scope.getTotal();
            $scope.updateCartStorage();
        };

        $scope.updateCartStorage = function () {
            localStorage.setItem('cartItems', JSON.stringify($scope.cartItems));
        };

        $scope.placeOrder = function () {
            var commitMenuItems = [];
            var commitUuid = [];
            var newUuid;
            var orderUuid = generateUUID();

            for (var i = 0; i < $scope.cartItems.length; ++i) {
                newUuid = generateUUID();
                commitMenuItems.push({
                    'id': 'NEW-demo$OrderItem-'+newUuid,
                    'menuItem': $scope.cartItems[i].menuItem,
                    'quantity': $scope.cartItems[i].quantity,
                    'order': {
                        'id': 'demo$Order-'+orderUuid
                    }
                });
                commitUuid.push({'id': 'demo$OrderItem-'+newUuid});
            }

            var commitInstances = [{
                'id': 'NEW-demo$Order-'+orderUuid,
                'orderItems': commitUuid
            }];

            $http({
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                url: APP_SERVER_URL + '/app-portal/api/commit',
                data: {
                    'commitInstances': commitInstances.concat(commitMenuItems)
                },
                params: {'s': localStorage.getItem('session_id')}
            }).then(function (response) {
                $scope.cartItems = [];
                $scope.updateCartStorage();
                checkCart();
                $scope.getTotal();
                $location.path('/orders');
            }, function (response) {
                if (response.status == 401) {
                    if (localStorage.getItem('username')) {
                        $rootScope.reauth(function(){
                            $scope.placeOrder()
                        });
                    }
                }
            });
        };

        $scope.removeFromCart = function (id) {
            for (var i = 0; i < $scope.cartItems.length; ++i) {
                if ($scope.cartItems[i].id == id)
                    $scope.cartItems.splice(i, 1);
            }
            checkCart();
            $scope.getTotal();
            $scope.updateCartStorage();
        };

        function checkCart() {
            $scope.emptyCart = $scope.cartItems.length == 0;
        }
    }


    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$rootScope', '$http', '$location', 'UserService'];
    function LoginController($rootScope, $http, $location, UserService) {
        var vm = this;
        vm.login = login;
        function login() {
            UserService.setCurrentUser({username:vm.username, password: vm.password});
            $http({
                method: 'GET',
                url: APP_SERVER_URL + LIFE_PORTLET_API_PREFIX + '/screenNode/getScreenNodeTemplate'

            }).then(function (response) {
                //localStorage.setItem('session_id', response.data.screenType);
                localStorage.setItem('username', vm.username);
                localStorage.setItem('password', vm.password);

                $location.path('/panel');
                $rootScope.checkLogin($rootScope);
            }, function (response) {
                UserService.setCurrentUser(null);
                if (response.status == 401) {
                    vm.authFailed = true;
                }
            });
        }
    }

angular
    .module('app')
    .controller('LogoutController', LogoutController);

LogoutController.$inject = ['$rootScope', '$http', '$location', 'UserService', '$cookies'];
function LogoutController($rootScope, $http, $location, UserService, $cookies) {

        $http({
            method: 'GET',
            url: APP_SERVER_URL + LIFE_PORTLET_API_PREFIX + '/screenNode/getScreenNodeTemplate'

        }).then(function (response) {
            //localStorage.removeItem('session_id');
            UserService.setCurrentUser(null);
            localStorage.removeItem('username');
            localStorage.removeItem('password');
            //localStorage.removeItem('cartItems');
            $cookies.put('JSESSIONID', "");
            $location.path('/login');
            $rootScope.checkLogin($rootScope);
        }, function (response) {
            $cookies.put('JSESSIONID', "");
            if (response.status == 401) {
                //vm.authFailed = true;
            }
        });

}

    angular
        .module('app')
        .controller('MenuController', MenuController);

    MenuController.$inject = ['$rootScope', '$scope', '$location', 'UserService'];
    function MenuController($rootScope, $scope, $location, UserService) {
        $scope.authAction = function () {
            if (UserService.getCurrentUser()) {
                $scope.logoutAction();
            } else {
                $scope.loginAction();
            }
        };

        $scope.logoutAction = function () {

            $rootScope.checkLogin($rootScope);
            $location.path('/logout');
        };

        $scope.loginAction = function () {
            $rootScope.checkLogin($rootScope);
            $location.path('/login');
        };

        $scope.homeAction = function () {
            if ($scope.authenticated) {
                $rootScope.checkLogin($rootScope);
                $location.path('/panel');
            }
            else {
                $scope.loginAction();
            }

        };
    }
//})();

function timeConverter(items) {
    for (var i = 0; i < items.length; ++i) {
        items[i].createTs = items[i].createTs.substring(0, items[i].createTs.indexOf('.'));
    }
    return items;
}

function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};